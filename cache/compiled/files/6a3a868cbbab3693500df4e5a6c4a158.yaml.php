<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/clients/client8/web19/web/user/plugins/frontend-edit-button/frontend-edit-button.yaml',
    'modified' => 1522159736,
    'data' => [
        'enabled' => true,
        'position' => 'tr',
        'showLabel' => true,
        'showIcon' => true,
        'iconSrc' => 'user/plugins/frontend-edit-button/images/icons/edit.png'
    ]
];
