<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/clients/client8/web19/web/user/config/plugins/seo.yaml',
    'modified' => 1520190188,
    'data' => [
        'enabled' => true,
        'article' => true,
        'restaurant' => true,
        'event' => true,
        'organization' => true,
        'musicevent' => true,
        'person' => true,
        'musicalbum' => true,
        'product' => true,
        'twitterid' => '@fair_coop',
        'twitter_default' => '1',
        'facebook_default' => '1'
    ]
];
