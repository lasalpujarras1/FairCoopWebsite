<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://fair-coop/fair-coop.yaml',
    'modified' => 1519806208,
    'data' => [
        'enabled' => true,
        'dropdown' => [
            'enabled' => true
        ]
    ]
];
