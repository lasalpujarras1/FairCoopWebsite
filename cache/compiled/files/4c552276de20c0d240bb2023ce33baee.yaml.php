<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/clients/client8/web19/web/user/config/site.yaml',
    'modified' => 1525709929,
    'data' => [
        'title' => 'FairCoop',
        'default_lang' => 'en',
        'author' => [
            'name' => 'Joe Bloggs',
            'email' => 'joe@test.com'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'The earth cooperative'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 220,
            'delimiter' => '==='
        ],
        'redirects' => [
            'error' => 'https://2017.fair.coop/'
        ],
        'blog' => [
            'route' => '/blog'
        ],
        'routes' => [
            '/tools' => '/faircoop-tools',
            '/herramientas' => '/faircoop-tools',
            '/join-us' => '/join'
        ]
    ]
];
