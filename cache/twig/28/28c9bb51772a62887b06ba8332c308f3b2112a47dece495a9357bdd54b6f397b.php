<?php

/* modular/nodes.html.twig */
class __TwigTemplate_2b4a192890210d6d6ef4ff7d14e77cd1377c4e67e0b6d161bed748e8666412cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"section-nodes\">
    <div class=\"container\">
\t<div class=\"col-md-8\">
        ";
        // line 4
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
\t</div>
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "buttons", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 7
            echo "        <a class=\"button";
            if ($this->getAttribute($context["button"], "primary", array())) {
                echo " primary";
            }
            echo "\" href=\"";
            echo $this->getAttribute($context["button"], "url", array());
            echo "\">";
            echo $this->getAttribute($context["button"], "text", array());
            echo "</a>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "
\t\t<div class=\"welcome\">
\t\t\t<span><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"#333\" width=\"18\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"1\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M4 19.5A2.5 2.5 0 0 1 6.5 17H20\"></path><path d=\"M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z\"></path></svg></span>
\t\t\t<h5>";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("How to create a local node");
        echo "</h5>
\t\t\t<div class=\"feed_actions\">
\t\t\t\t<a class=\"inline\" href=\"https://fair.coop/docs/how-to-create-local-nodes/\" target=\"_blank\" >";
        // line 14
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("Read online");
        echo "</a>
\t\t\t</div>
\t\t</div>
\t
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/nodes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 14,  53 => 12,  48 => 9,  33 => 7,  29 => 6,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"section-nodes\">
    <div class=\"container\">
\t<div class=\"col-md-8\">
        {{ page.content }}
\t</div>
    {% for button in page.header.buttons %}
        <a class=\"button{% if button.primary %} primary{% endif %}\" href=\"{{ button.url }}\">{{ button.text }}</a>
    {% endfor %}

\t\t<div class=\"welcome\">
\t\t\t<span><svg xmlns=\"http://www.w3.org/2000/svg\" color=\"#333\" width=\"18\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"1\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"><path d=\"M4 19.5A2.5 2.5 0 0 1 6.5 17H20\"></path><path d=\"M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z\"></path></svg></span>
\t\t\t<h5>{{ 'How to create a local node'|t }}</h5>
\t\t\t<div class=\"feed_actions\">
\t\t\t\t<a class=\"inline\" href=\"https://fair.coop/docs/how-to-create-local-nodes/\" target=\"_blank\" >{{ 'Read online'|t }}</a>
\t\t\t</div>
\t\t</div>
\t
    </div>
</section>
", "modular/nodes.html.twig", "/var/www/clients/client8/web19/web/user/themes/fair-coop/templates/modular/nodes.html.twig");
    }
}
