<?php

/* partials/edit-button.html.twig */
class __TwigTemplate_310a4e542d9b511a6a2c48f1f5ce1c110edd372ad47c639df0a115a03fbb13a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["_showIcon"] = $this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute(($context["config"] ?? null), "showIcon", array()), true);
        // line 2
        $context["_showLabel"] = $this->env->getExtension('Grav\Common\Twig\TwigExtension')->definedDefaultFilter($this->getAttribute(($context["config"] ?? null), "showLabel", array()), true);
        // line 3
        echo "
";
        // line 4
        if (((($context["_showLabel"] ?? null) == false) && (($context["_showIcon"] ?? null) == false))) {
            // line 5
            echo "    ";
            $context["_showLabel"] = true;
        }
        // line 7
        echo "
<div id='frontend-edit-button' class='";
        // line 8
        echo ($context["vertical"] ?? null);
        echo " ";
        echo ($context["horizontal"] ?? null);
        echo "'>
    <div class='page-item' data-nav-id='/grav-cms";
        // line 9
        echo ($context["pageUrl"] ?? null);
        echo "'>
        <a class='page-edit frontend-edit-button' target='_blank' href='";
        // line 10
        echo ($context["editUrl"] ?? null);
        echo "'>
            ";
        // line 11
        if ((($context["_showIcon"] ?? null) == true)) {
            // line 12
            echo "                <i class='icon-feb-editor'></i>
            ";
        }
        // line 14
        echo "            ";
        if ((($context["_showLabel"] ?? null) == true)) {
            // line 15
            echo "                <span class=\"label\">
                    ";
            // line 16
            echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("PLUGIN_FRONTEND_EDIT_BUTTON.BUTTON_TEXT");
            echo "
                </span>
            ";
        }
        // line 19
        echo "        </a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "partials/edit-button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 19,  61 => 16,  58 => 15,  55 => 14,  51 => 12,  49 => 11,  45 => 10,  41 => 9,  35 => 8,  32 => 7,  28 => 5,  26 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _showIcon = config.showIcon|defined(true) %}
{% set _showLabel = config.showLabel|defined(true) %}

{% if _showLabel == false and _showIcon == false %}
    {% set _showLabel = true %}
{% endif %}

<div id='frontend-edit-button' class='{{ vertical }} {{ horizontal }}'>
    <div class='page-item' data-nav-id='/grav-cms{{ pageUrl }}'>
        <a class='page-edit frontend-edit-button' target='_blank' href='{{ editUrl }}'>
            {% if _showIcon == true %}
                <i class='icon-feb-editor'></i>
            {% endif %}
            {% if _showLabel == true %}
                <span class=\"label\">
                    {{ 'PLUGIN_FRONTEND_EDIT_BUTTON.BUTTON_TEXT'|t }}
                </span>
            {% endif %}
        </a>
    </div>
</div>", "partials/edit-button.html.twig", "/var/www/clients/client8/web19/web/user/plugins/frontend-edit-button/templates/partials/edit-button.html.twig");
    }
}
