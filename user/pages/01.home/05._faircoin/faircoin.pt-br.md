---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin Mapa Mundial'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Usamos a
## **FairCoin**
Como uma moeda digital ética, a FairCoin é impulsionada pelos valores do ecossistema da FairCoop e apoiada por um movimento colaborativo global de rápido crescimento. Como qualquer outra criptomoeda, ela é descentralizada. Ao mesmo tempo, porém, a FairCoin implementa inovações radicais que a tornam única em termos de operação ecológica, crescimento estável de valor, comércio ético, poupanças e oportunidades de financiamento. Seu desenvolvimento continua com o objetivo de criar um sistema econômico justo para o planeta. Leia mais sobre a [FairCoin](https://fair-coin.org/)

## para criar uma
## ** economia circular**
Estamos construindo uma economia real e produtiva em torno da FairCoin, com a criação de um modelo circular para o uso de produtos e serviços, tanto a nível local como a nível global. Cada vez mais comerciantes, lojistas e produtores em todo o mundo confiam na FairCoin como meio de troca, aproveitando seu valor estável e as várias ferramentas abertas e gratuitas projetadas para apoiar a moeda.