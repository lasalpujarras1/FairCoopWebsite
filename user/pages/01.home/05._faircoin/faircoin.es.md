---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin en el mundo'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Usamos 
## **FairCoin**
FairCoin es una moneda digital ética, que se rige por los valores del ecosistema FairCoop y que cuenta con el apoyo de un movimiento cooperativo global que crece rápidamente. Es descentralizada, igual que cualquier otra criptomoneda, pero, al mismo tiempo, implementa innovaciones radicales que la hacen única en términos de funcionamiento ecológico, crecimiento de valor estable, comercio ético y oportunidades de ahorro y financiación. Su desarrollo persigue el objetivo de crear un sistema económico justo para todo el planeta.[ Leer más.](https://fair-coin.org/)



## para construir 
## **Economía circular**
Estamos construyendo una economía real y productiva en torno al FairCoin, con la creación de un uso circular de productos y servicios, tanto a escala local como global. Cada vez son más los comercios, tiendas y productores de todo el mundo que confían en FairCoin como medio de intercambio, aprovechando la estabilidad de su valor y las distintas herramientas abiertas y libres que se han diseñado para apoyarlo: