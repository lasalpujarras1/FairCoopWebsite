---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'Faircoin nel mondo'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Noi usiamo 
## **FairCoin**
Il FairCoin è una moneta digitale etica, guidata dai valori dell'ecosistema FairCoop e promossa da un movimento cooperativo globale in rapida crescita. È decentralizzata, come ogni altra valuta digitale, ma al tempo stesso realizza innovazioni radicali che la rendono unica in termini di funzionamento ecologico, crescita stabile del valore, commercio etico, risparmio ed opportunità di finanziamento. Il suo sviluppo continua con l'obiettivo di creare un sistema economico equo per la Terra. [Più informazione: ](https://fair-coin.org/)



## per costruire una
## **Economia circolare**
Stiamo costruendo un'economia reale e produttiva che ha al centro la moneta cooperativa FairCoin che implica la circuitazione di prodotti e servizi, a livello locale e globale. Sempre più commercianti, negozi, lavoratori e produttori di tutto il mondo si fidano di FairCoin come mezzo di scambio, sfruttando il suo valore stabile ed i vari strumenti aperti e gratuiti progettati per supportarlo.