---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin per tot el món'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Utilitzem 
## **FairCoin**
FairCoin és una moneda digital ètica guiada pels valors de l'ecosistema FairCoop i recolzada per un moviment cooperatiu global de ràpid creixement. És una moneda descentralitzada, com qualsevol altra criptomoneda, però al mateix temps implementa innovacions radicals que la fan única en termes de funcionament ecològic, creixement de valor estable, comerç just, estalvis i oportunitats de finançament. El seu desenvolupament continua amb l'objectiu de crear un sistema econòmic just per al planeta.[Pots llegir més.](https://fair-coin.org/)



## per a construir
## **economia circular**
Estem construint una economia real i productiva al voltant del FairCoin, amb la creació d'un ús circular de productes i serveis tant a nivell local com global. Cada volta més comerciants, tendes i productors de tot el món confien en FairCoin com a mitjà d'intercanvi, aprofitant-se del seu valor estable i de les diferents eines lliures dissenyades per a recolzar-lo.