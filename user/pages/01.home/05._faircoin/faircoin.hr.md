---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoins worldwide'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## We use 
## **FairCoin**
We use 
FairCoin
FairCoin is an ethical digital currency, driven by the values of the FairCoop ecosystem and supported by a fast-growing global cooperative movement. It is decentralized, as with any other cryptocurency, but at the same time implements radical innovations that makes it unique in terms of ecological operation, stable value growth, ethical trade, savings and funding opportunities. Its development continues with the goal of creating a fair economic system for the Earth. Read more about[ Read more.](https://fair-coin.org/)



## to build 
## **Circular Economy**
We are building a real and productive economy around FairCoin, with the creation of a circular use of products and services either at local or at a global level. More and more merchants, shops and producers worldwide trust faircoin as a means of exchange, taking advantage of its stable value and the various open and free tools designed to support it.