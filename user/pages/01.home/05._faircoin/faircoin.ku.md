---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'ferkoyn derdora cîhan'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Em FairCoin 
## **FairCoin**
FairCoin (ferkoyn) cûrepereyek ehlaqî yê dijîtale, ji aliyê nirxên ekosîstema FairCoinê ve tê meşandin û ji aliyê bizavek koperatîva mezin ya cîhanî ya mezin-lez ve tê piştgirîkirin . Ew ne navendî ye, wek bi her cûrepereyên din yên krîpto , di heman demê de nûbînên radîkal cih bi cih dike daku wê bi mercên jîngehî re bike yek, herwuha mezinbuna nirxê sabît , bazirganiya ehlaqî , tomarkirin û fînansekirina derfetan pêktîne. Pêşketina wê girêdayîye bi afirandina sîstemek aborî ya adîlane ya cîhanî ku ji bo cîhanê bê damezrandin.[Faircoin](https://fair-coin.org/)



## ji bo 
## **Aborîya Çemberî (Bazinî) avabikin**
Em li derdora FairCoinê aborîyek berhemdar û rasteqîn ava dikin, em bi afirandina çemberî ya bikaranîna berhem û xizmetguzariyan re yan di asta herêmî anjî cîhanî de vê pêktînin. Herî zêde bazirgan, dukkan û berhemhînerên seranserê cîhanê baweriya wan bi FairCoin tên weku malek yê bazarkirinê, bi sûdmendiya ji nirxê wê yê xwegir-sabît û amêrên cuda cuda yên serbest û vekirî ji bo wê hatine dizaynkirin û piştgiriya wê dikin.