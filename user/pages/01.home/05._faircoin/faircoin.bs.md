---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin-i svuda u svijetu'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Mi koristimo 
## **FairCoin-e**
FairCoin je etička digitalna valuta, vođena vrijednostima FairCoop ekosistema i podržana brzo-rastućim globalnim kooperativnim pokretom. Decentralizovan, kao i svaka druga kriptovaluta, ali u isto vrijeme implementira radikalne inovacije koje je čine jedinstvenom u smislu ekoloških operacija, stabilnog rasta vrijednosti, etične trgovine, štednje i prilike za finansiranje. Razvoj valute se nastavlja sa ciljem kreiranja fer/pravednog ekonomskog sistema za planetu Zemlju.[Pročitati više](https://fair-coin.org/)



## izgradnja 
## **Cirkularne/Kružeće Ekonomije**
Gradimo realnu i produktivnu ekonomiju vezanu na FairCoin, sa stvaranjem kružeće/cirkularne upotrebe proizvoda i usluga na lokalnom ili globalnom nivou. Sve više i više trgovaca, prodavnica i proizvođača širom svijeta vjeruju faircoin-u kao sredstvu za razmjenu, koristeći prednost stabilne vrijednosti i različitih besplatnih i otvorenih alata napravljenih u svrhu podrške.