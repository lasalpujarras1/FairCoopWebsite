---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'FairCoin-i širom sveta'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Koristimo 
## **FairCoin**
FairCoin je etička, digitalna valuta, koju pokreću vrednosti FairCoop ekosistema i koja je podržana od strane brzorastućeg globalnog kooperativnog pokreta. Ona je decentralizovana, kao i druge kriptovalute, ali istovremeno uvodi radikalne inovacije koje je čine jedinstvenom u smislu ekološke upotrebe, stabllnog rasta vrednosti, mogućnosti štednje i finansiranja. FairCoin nastavlja da se razvija sa ciljem stvaranja pravičnog ekonomskog sistema. Pročitajte više na [Faircoin](https://fair-coin.org/)



## Izgradnja
## **cirkularne ekonomije**
Gradimo stvarnu i produktivnu ekonomiju oko Faircoin-a, cirkularnom upotrebom proizvoda i usluga, bilo na lokalnom ili globalnom nivou. Sve više trgovaca, prodavnica i proizvođača, širom sveta, kao sredstvo razmene upotrebljavaju Faircoin, koristeći prednosti njegove stabilne vrednosti i raznih otvorenih i slobodnih instrumenata za podršku.