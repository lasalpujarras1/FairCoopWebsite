---
title: Faircoin
buttons:
    -
        text: Fairmarket
        url: 'https://market.fair.coop/'
        primary: true
    -
        text: 'Faircoins dans le monde'
        url: 'https://use.fair-coin.org/'
        primary: false
---

## Nous utilisons 
## **FairCoin**
Le FairCoin est une monnaie numérique éthique, portant les valeurs de FairCoop et supportée par une coopérative globale à forte progression. Elle est décentralisée, comme les autres crypto-monnaies mais, en même temps, elle contribue à implémenter   des innovations radicales qui la rendent unique en terme écologique, avec la stabilité de sa valeur, des échanges, une épargne et des placements éthiques. Son développement continue dans le but de créer un système économique équitable pour notre terre. [En savoir plus](https://fair-coin.org/)



## Pour construire une 
## **Economie circulaire**
Nous construisons une vraie économie autour du FairCoin avec une utilisation circulaire locale et globale des produits et services. De plus en plus de commenrçants, de producteurs et de magasins dans le monde font confiance aux FairCoins comme valeur d'échange et bénificie de sa valeur stable et des divers outils libres et gratuits créés spécialement pour cette monnaie.