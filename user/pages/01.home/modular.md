---
title: Home
menu: Home
onpage_menu: false
content:
    items: '@self.modular'
    order:
       by: default
       dir: asc
       custom:
            - _intro
            - _values
            - _image
	    - _method
	    - _faircoin
---
