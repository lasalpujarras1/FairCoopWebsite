---
title: '# Quelques étapes majeures que nous avons déjà atteintes'
---

* Création et valorisation d' ne crypto-monnaie unique au monde (le FairCoin)
* Démarrage d'un réseau de noeuds locaux en expansion rapide (LN)
* Création d'une coopérative européenne (FreedomCoop)
* Co-foundation d'une banque coopérative (Bank of the Commons)
* Construction d'un marché en-ligne (FairMarket) et une plateforme collaborative et libre (OCP)
* Pas de morts, ni de blessés (nous nous sommes pas encore tous entretués) ;)