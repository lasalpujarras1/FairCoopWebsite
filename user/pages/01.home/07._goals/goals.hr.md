---
title: '# Some of the major milestones we have achieved so far'
---

* Made and are using an innovative, secure and ecological currency (FairCoin)
* Started a fast-growing global network of local nodes
* Opened a European cooperative framework (FreedomCoop)
* Co-founded a cooperative bank (Bank of the Commons)
* Built an online marketplace (FairMarket) and a collaborative working platform (OCP)
* Haven't killed each other (yet) ;)