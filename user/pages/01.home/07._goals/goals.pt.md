---
title: '# Alguns dos maiores objectivos que atingimos até agora'
---

* Criar e usar uma moeda inovadora, segura e ecologica (FairCoin)
* Iniciamos uma rede global de nós local em rapido crescimento.
* Abrimos uma Cooperativa Europeia (FreedomCoop)
* Somos Co-fundadores de um banco cooperativo (Bank of the Commons)
* Construimos um mercado online (FairMarket) e uma plataforma de trabalho colaborativo (OCP)
* E ainda não nos esfolamos uns aos outros (ainda) ;)