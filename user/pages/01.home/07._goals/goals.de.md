---
title: '# Wichtige Meilensteine, die wir bisher erreicht haben'
---

* Entwicklung einer innovativen, sicheren und ökologischen Währung (FairCoin)
* Start eines schnell wachsenden globalen Netzwerks von lokalen Knotenpunkten
* Schaffung eines europäischen Genossenschaftsrahmens (FreedomCoop)
* Mitbegründung einer Genossenschaftsbank (Bank of the Commons)
* Aufbau eines Online-Marktplatzes (FairMarket) und einer kollaborativen Arbeitsplattfrom (OCP)
* Haben uns (noch) nicht gegenseitig umgebracht ;)