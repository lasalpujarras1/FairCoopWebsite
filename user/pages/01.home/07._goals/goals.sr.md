---
title: '# Prekretnice u našem radu'
---

* Kreirana je, i u upotrebi, inovativna, sigurna, ekološka valuta (FairCoin)
* Započet je brz globalni rast mreža lokalnih petlji
* Otvoren je Evropski kooperativni okvir (FreedomCoop)
* Osnovana je kooperativna banka (Bank of the Commons)
* Izgrađeno je onlajn tržište (FairMarket) i platforma za kooperativni rad (OCP)
* Nismo se međusobno poubijali (još uvek) ;)