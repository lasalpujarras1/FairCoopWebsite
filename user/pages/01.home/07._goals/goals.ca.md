---
title: '# Algunes de les fites aconseguides fins ara'
---

* Hem creat i estem utilitzant una innovadora moneda ecològica i segura
* Hem engegat una xarxa de nodes locals que ha crescut ràpidament
* Hem posat en marxa una estructura cooperativa europea (FreedomCoop)
* Hem cofundat un banc cooperatiu (Bank of the Commons)
* Hem construït un mercat online (FairMarket) i una plataforma de treball col·laboratiu (OCP)
* No ens hem matat les unes a les altres (encara) ;)