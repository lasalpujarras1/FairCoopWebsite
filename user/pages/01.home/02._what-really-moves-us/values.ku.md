---
title: '# Tevgera me çiye?'
values1:
    heading: 'Şoreşa Întegral'
    description: 'Proseyek ya nirxên dîrokî ye, ji bo avakirina civakek bikare xwe rêve bibe, li ser bingeha xweserîyê ye û ji bo ji holê rakirina hemî formên desthalatiyê: dewlet, kapîtalîzm, û hemî formên din yên ku bandorê li têkiliya di navbera mirov û jîngehê de çêdikine. Xwe li ser tevgera wijdanî, kesî û kolektîv ava dike. Ji bo dubare paşve anîna nirx û yeksaniyan ya jiyanek hevbeş hewl dide. Li ser bingeha avahîsaziyên nû ye û formên xwe rêxistinkirinê di her qadek ya jiyanê de bi armanca dilniyabûna di biryar dayîn û yeksaniya civînan de, evane pêwîstiyên me yên bingehîn û jiyanîne.'
values2:
    heading: Asetî
    description: 'Em hinek caran ferman ji hikumetan napejirînin. Hinek caran ev erka me ye, ji ber ku qanûn hemberî gele. Ji bo vî fêm bikin, em cudahiya navbera qanûnî (fermî) û adîl dibînin. Çend qanûnên dewlet ne adîlin. Ev bin destdhilatdariya dewlet wisa ye. Mînak, dema dewletek pereya komînal ji bo şer û êrîşên ''qanûnî'', an jî dijî civak bi kar tîne, em vê napejirînin.'
values3:
    heading: 'Koperatîva Vekirî'
    description: 'Em bingehan awayên koperatîv dişopînin û piştgirî dikin, ji ber ku hêza wan ji endam tê, û nirxên wan birayetî, xweseriya xwe, wekhevî, demokrasî, û piştevanî ne. Her kes dikare li ser biryar û polîtîkayên pêkanîna çalak û wekhev beşdar bibin. Milkiyeta me, piranî bi rêjeya zêdekirina pereyê me yê ferkoyn, di pêşveçûna ekosîstem û projeyên FC de tê veguhestin. Bi vê rêyê beşdar dikare xwe bigehîne amêrên kesî yên bihêztir û bi berdewam bigihe tora mezinbuna malan-berheman û di Aboriyek Adîlane de xizmetê bike.'
values4:
    heading: Bênavendîtî
    description: 'Armancek ya me, rêxistina bênavendî ji bo modela biryar û role. Bi rêya belavkirina teknolojiya me yek xala binketî berbibînin, yanî desthiltdar nikare me bigire. Em dixwazin zelal bikin ku hemî beşdar dikarin bê bindestî û bi azadî biaxivin, fikrên hevpar, xebatên li ser projeyan an nirxan veguhêzin. Belavkirina hêza siyasî û aborî, bi hev re bi zelaliya radîkal, nebaşkaranîna ji rol bi berjewendî yekane disekine û ji ber vî qelpezanî, bertîlgirî û desthilatdarî nabe.'
values5:
    heading: 'Konfederalîzma Demokratîk'
    description: 'Bingeha modernîta kapîtalîst sîstema dewlete, û bingeha wê serdama baveksalarî û hiyerarşîke, ku kûr xwe avêtibû nava civakê, piştî sedsalan ku mirov ser mirov bindest kirin, cara yekemîn di dîrokê de, hişmendiya siyasî, kêmbûna aboriya kapîtalîst û teknolojiya kriptografiya p2p bi hev re qabul dipejirînin ku em alternatîfên cidî û rastî ji modela rêveberiya heyî re çpdikin. Pergalek aborî ya hevkariya cîhanî ji me re sûkên azad dide, û ev pêngavek mezine. Heke ev bi hev re bi sîstemeke nirxên exlaqî û azadî ya berbiçav re bibe yek, û bi protokola biryara demokratîk re xwebigehîne hevdu, ewê hêdî hêdî bibe rastî. Heger tu bawer dikî ku jiyan ji derveyê dewletê dibe, vaye FairCoop tevlî bibe.'
---

