---
title: 'una capbussada en la nostra metodologia'
media_order: diagram.268de66e.jpg
---

# Assemblees obertes i consens
En les assemblees obertes, la comunitat decideix com abastar els objectius comuns. Tota persona pot participar i fer propostes de desenvolupaments, canvis o projectes en els que treballar. Quan hi ha consens en una proposta, la persona que l'ha feta i els grups de treball relacionats amb el tema es posaran en acció. Ens organitzem així tant en les reunions generals online com en les físiques dels nodes locals.

# Auto-organització i Distribució del Valor
L'ecosistema FairCoop s'organitza a través de. Animem a tota persona a participar amb les seues pròpies idees i habilitats i a assumir responsabilitats de forma proactiva, ajudant per tant al creixement del moviment. Basant-se en la nostra innovadora eina de treball col·laboratiu, el Open Collaborative Platform les participants poden aportar el seu treball voluntàriament o demanar remuneració en FairCoins per a cobrir les seues necessitats. D'aquesta manera, recompensem qui està aportant valor amb el seu treball en FairCoop, i la distribució de valor es fa de forma justa i descentralitzada.