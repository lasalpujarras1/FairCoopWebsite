---
title: 'Upustite se u našu metodologiju'
media_order: diagram.268de66e.jpg
---

# # Otvoreni skupovi i konsenzus
Na otvorenim skupovima, zajednica odlučuje o načinu postizanja osnovnih ciljeva. Svako može da učestvuje i da daje predloge za razvoj, promene ili projekte. Kada predlog prođe konsenzus, podnosilac predloga i relevantna radna grupa će ga sprovesti u delo. Na ovaj način organizujemo i globalne onlajn sastanke i fizičke susrete učesnika u radu lokalnih petlji.

# # Samoorganizovanje i raspodela dobiti
FairCoop ekosistem se organizuje kroz . Svi su ohrabreni da učestvuju sa individualnim idejama i veštinama, i tako preuzmu proaktivnu odgovornost, kako bi pomogli da se pokret širi. Upotrebom našeg inovativnog, kolaborativnog radnog instrumenta, učesnici mogu da evidentiraju volonterske radne sate, ili da zahtevaju Faircoin-e za pokriće svojih potreba. Na ovaj način se nagrađuju oni koji radom doprinose razvoju FairCoop-a, a dobit se distribuira na pravičan i decentralizovan način.