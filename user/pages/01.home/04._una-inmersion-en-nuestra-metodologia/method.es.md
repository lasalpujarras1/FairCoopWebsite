---
title: 'Una inmersión en nuestra metodología'
media_order: diagram.268de66e.jpg
---

# #Asambleas abiertas y consenso
En las asambleas abiertas, la comunidad decide cómo llegamos a las metas comunes. Cualquiera puede participar en ellas y realizar propuestas de desarrollos, cambios o proyectos en los que trabajar. Cuando una propuesta se aprueba por consenso, quien la haya propuesto y los grupos de trabajo relevantes para la misma, se ponen manos a la obra. De esta forma organizamos tanto las reuniones globales online, como las reuniones físicas de los nodos locales.

# #Autoorganización y distribución de valor
El ecosistema FairCoop se organiza mediante el cooperativismo abierto. Se anima a todo el mundo a participar con sus habilidades e ideas particulares y a ser proactivas en la asunción de responsabilidades, ayudando, de esta forma, a que el movimiento crezca. A través de nuestra innovadora herramienta de trabajo colaborativo (OCP), las participantes pueden registrar el tiempo dedicado al proyecto, bien de forma voluntaria, bien solicitando una remuneración en FairCoin para cubrir sus necesidades. De esta forma, recompensamos a quienes están aportando un valor real a FairCoop con su trabajo y distribuimos el valor de una forma justa y descentralizada.