---
title: 'Rêbaza me bixwîne'
media_order: diagram.268de66e.jpg
---

# # Vekirina Meclîsan û Lihevhatin
Di vekirina meclîsan de, civat ango komîn biryarê dide ka çawa xwe bigehînin serkeftinên me. Herkes dikare tevilbun çêke û ji bo pêşketin, guhertin û projeyan pêşniyaran bike ka çawa dikare xebatê bimeşînin. Dema pêşniyarek bi lihevhatinê di raya komînê re derbaz bû, wê demê pêşniyarker û desteyên peywendîdar wê bikevin nava bizavê de. Em bi vê rêyê rêxistinbûna civînên cihanî yên li ser xetê û civînên nodên herêmî (NH) yên bi fizîkî çêdikin.

# # Xwe Rêxistinkirin û Belavkirina Nirxan
Ekosîstema FairCoop xwe bi rêya Koparatîvîzma Vekirî rêxistin dike. Herkes dikare bi raman û kêrhatinên xwe yên kesî têde beşdarî bike, berpirsyartiyên proaktif (çalakane) bigre ser milê xwe, bi vî awayî alîkarî bide mezinbuna vê bizavê. Li ser bingehê amêrên me yên nû û hevkarî [LINK|Platforma Hevkariya Vekirî] beşdar yan dikare demjimêrên xwe bi dilxwazî bikarbîne anjî ji faircoin derkeve ji bo pêwîstiyên wana bicih bîne. Bi vê rêyê em wan kesan xelat dikin ku bi xebata xwe nirxek rastî tînin nava FairCoopê û belavkirina nirxan bi rêyek adîlane û ne navendî tê encam dan.