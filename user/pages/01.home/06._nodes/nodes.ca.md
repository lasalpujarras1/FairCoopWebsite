---
title: Nodes
buttons:
    -
        text: 'Dóna-li una ullada a la creixent xarxa'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Nodes locals</strong></h2>

A més de la visió global, molts nodes locals estan afegint-se a la xarxa FairCoop per tot el món, per a connectar a l'ecosistema les iniciatives, els col·lectius i les persones de cada territori. Aquesta és una relació bidireccional que recolza i fa realitat els objectius de FairCoop en les comunitats locals, alhora que aporta un important feedback. És una xarxa d'ajuda mútua construïda per a expandir el nostre sistema econòmic al nivell global, construint confiança local i recolzant l'activitat econòmica real.