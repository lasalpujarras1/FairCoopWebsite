---
title: Nodi
buttons:
    -
        text: 'Guarda la mappa del nostro network'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong># I nodi locali</strong></h2>

Alla rete globale di FairCoop si aggiungono sempre più nodi locali in tutto il mondo, così da collegare le iniziative locali, i gruppi e le persone all'ecosistema internazionale. Questa relazione bidirezionale aiuta a realizzare gli obiettivi di FairCoop nelle comunità locali, ricevendo in cambio importanti risposte, stimoli, sostegno, pratiche. Si tratta di una rete di mutua assistenza che è stata creata per espandere il nostro sistema economico a livello globale, attraverso la costruzione di relazioni basate sulla fiducia a livello locale ed il sostegno di un'attività economica reale.