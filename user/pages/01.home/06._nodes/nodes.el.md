---
title: Κόμβοι
buttons:
    -
        text: 'Ανακαλύψτε το αναπτυσσόμενο αυτό δίκτυο'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong># Τοπικοί Κόμβοι</strong></h2>

Πέρα από το παγκόσμιο όραμα του FairCoop, συνεχώς νέοι Τοπικοί Κόμβοι δημιουργούνται σε όλον τον κόσμο, προκειμένου να συνδέσουν πρωτοβουλίες, ομάδες και άτομα με το οικοσύστημα. Πρόκειται για μια αμφίδρομη σχέση, ένα δίκτυο αμοιβαίας βοήθειας και σχέσεων εμπιστοσύνης, ανταλλαγής γνώσεων και πληροφοριών, που ενισχύει τις οικονομικές δραστηριότητες σε τοπικό επίπεδο κι ευνοεί την επέκταση του οικονομικού μας συστήματος σε παγκόσμιο επίπεδο.