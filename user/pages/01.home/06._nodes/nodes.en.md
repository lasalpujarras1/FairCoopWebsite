---
title: Nodes
buttons:
    -
        text: 'Check out the growing network'
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Local Nodes </strong></h2>

Apart from the global vision, multiple Local Nodes are being added to the FairCoop network all around the world, in order to connect local initiatives, groups and people to the ecosystem. This is a bidirectional relation that supports and realizes FairCoop’s goals in local communities while returning important feedback. It is a mutual aid network built to expand our economic system at the global level, through building local trust and supporting real economic activity.