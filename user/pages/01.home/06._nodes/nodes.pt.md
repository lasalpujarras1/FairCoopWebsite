---
title: 'Nós Locais'
buttons:
    -
        text: 'Vê a rede em crescimento '
        url: 'http://map.fairplayground.info/map-localnodes/'
        primary: true
---

<h2><strong> # Nós Locais</strong></h2>

Para além da sua visão global, multiplos Nós Locais estão a crescer na rede FairCoop pelo mundo inteiro, com o objectivo de ligal iniciativas locais, grupos e pessoas ao ecosistema global. Esta é uma relação bidireccional  que suporta e realiza os objectivos da FairCoop nas comunidades locais. É uma rede de ajuda mutua para expandir o nosso sistema economico ao nivel global, atraves da criação de uma rede local de confiança e supporte real da actividade economica.
