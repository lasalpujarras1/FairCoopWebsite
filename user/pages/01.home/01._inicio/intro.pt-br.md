---
title: Home
media_order: homeback.png
heading: 'O QUE VEM DEPOIS DO CAPITALISMO?'
background_image: homeback.png
---

<h1>O ecossistema
<br>
da cooperativa global
<br>
para uma economia justa</h1>