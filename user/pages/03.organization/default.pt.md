---
title: Organização
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

A comunidade FairCoop organiza-se através do cooperativismo aberto. Todas as pessoas do mundo estão convidadas a participar com as competências que têm e que podem fazer com que o movimento cresça.

Como trabalhamos globalmente para apoiar a descentralização e a economia local, o nosso plano é seguir um modelo de organização glocal ( combinação dos âmbitos globais e locais ):

## Áreas de Trabalho Global (Open Coop Work)
Referem-se principalmente ao trabalho online, orientado a estruturar, organizar e desenvolver informação e tecnologia para a rede global. Todas as tarefas são organizadas na nossa plataforma colaborativa e aberta ( OCP-Open Collaborative Platform). As pessoas que participem podem trabalhar de forma voluntária ou solicitar uma remuneraçao em FairCoin pelo trabalho que estejam a desenvolver.

As áreas de trabalho global (OCW-Open Coop Work) são
* Gestão Comum
* Comunicação
* Economia Circular
* Desenvolvimento Técnológico
*Acolhimento-Educação-Boas vindas

[Mais sobre as Áreas de Trabalho Global Aqui](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Trabalho dos Nós Locais 
Refere-se às actividades online e offline que se realiza em cada um dos nós locais para desenvolver uma rede física sobre o território, centrando-se no trabalho regional individual. Algumas das competências do nós locais poderiam ser :
* Dinamizar a economia circular ( apoio ao comércio)
* Promover diferentes ferramentas da FairCoop
* Gerir um Ponto de Cambio (PoE-Point of Exchange)
* Promover os valores e as práticas da FairCoop entre os projecto locais
* Construir projectos locais específicos dentro do ecossistema da FairCoop

Cada área de trabalho global e cada nó local dispõe de autonomia para tomar decisões internas na hora de levar a cabo as suas tarefas e cumprir com os seus objectivos da melhor forma possível. Desta maneira, não são necessárias permissões centralizadas. Não obstante, os assunto chave que afectam toda a comunidade são decididos na assembleia geral global, como sempre, por consenso.

Todas as áreas de trabalho local e todos os nós locais dispõem de distintos chats e subáreas para a sua comunicação diária e a sua coordenação.

Se te interessa participar neles, por favor, junta-te ao grupo de benvinda e contacta-nos directamente. Alguém no grupo pode redireccionar o teu pedido ou dúvida para um local mais apropriado para os teus interesses e funções .

[Mais Detalhes sobre os Nós Locais Aqui.](https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D)

## Rede de Nós Locais 
Os nós locais são os grupos de representação regional da FairCoop a nível local. Eles desenvolvem e ampliam a rede regional de maneira independente, promovendo as ferramentas da FairCoop entre as lojas e pessoas das sua localidade, proporcionando apoio técnico, fazendo apresentações, respondendo a todo o tipo de dúvidas e inclusivamente iniciando os seus próprios projectos de um nó local dentro dos princípio da FairCoop. Os nós locais podem receber um pequeno financiamento para cobrir os seus custos básicos, com a ideia de que com o tempo, se tornem auto suficientes. 

Para criar um nó local, necessitam de pelo menos de três participantes activos. A partir daqui todas as pessoas interessadas que entrem em contacto directo com o nó local da sua região. Podes encontrar o mapa de nós locais aqui com mais detalhe aqui. Se não encontras um nó local na tua zona, estaremos disponíveis para te ajudar e apoiar para que cries um .

[Mais Detalhes sobre os Nós Locais Aqui.](https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D)