---
title: Organizacija
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Zajednica FairCoop-a se organizuje kroz otvoreni kooperativizam. Svi su potaknuti da učestvuju sa svojim individualnim vještinama i pomognu rast pokreta.

Kako radimo globalno da podržimo decentralizaciju i lokalnu ekonomiju, naš pristup je da slijedimo glokalni(globalno-lokalni) model organizacije:

## Globalne Radne oblasti
se odnose uglavnom na internet rad-online, ciljajući na strukturu, organiziranje i razvoj informacija i tehnologije za globalnu mrežu. Svi zadaci su organizovani na našoj Otvorenoj Saradničkoj Platformi (OCP). Učesnici mogu volontirati ili zahtijevati naknadu u FairCoin-ima za posao koji se treba obaviti.

Naše Globalne Radne oblasti su:
* Zajednički Menadžment
* Komunikacija
* Cirkularna Ekonomija
* Razvoj Tehnologije
* Dobrodošlica-Edukacija-Podrška

[Više O Našoj Strukturi Globalnih Radnih Oblasti, Ovdje](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Rad Lokalnih Čvorova
odnosi se i na online i offline djelovanja koje vode lokalni čvor ka razvoju mreže pojedinaca na terenu sa fokusom na pojedinu regionalnu djelatnost. Neke od oblasti Lokalnih Čvorova mogle bi biti:

* Dinamizacija cirkularne ekonomije (podrška trgovcima)
* Promocija različitih FairCoop Alata
* Upravljanje Point of Exchange (PoE) - Mjenjačnice
* Promoviranje vrijednosti i praksi FairCoop-a u lokalnim projektima
* Pravljenje određenih lokalnih projekata unutar FairCoop ekosistema

Svaka Globalna Radna Oblast i Lokalni Čvor imaju autonomiju donošenja unutrašnjih odluka kroz skupštine da bi ispunili svoje zadatke i postigli svoje ciljeve na najbolji način. Stoga, centralizirane dozvole nisu potrebne. Međutim, glavni problemi, koji se odnose na cijelu zajednicu, rješavaju se na generalnim globalnim skupštinama, putem koncenzusa.

Sve globalne radne oblasti i oblasti lokalnih čvorova imaju različite grupe za razgovor i podoblasti za dnevnu komunikaciju i koordinaciju.

Ako ste zainteresovani za učestvovanje, molimo uključite se u našu [grupu dobrodošlice](https://t.me/askfaircoop)te stupite u direktan kontakt. Odatle, možemo vam naći mjesto koje najbolje odgovara vašim interesima i namjerama.

## Mreža Lokalnih Čvorova
Lokalni Čvorovi su regionalne predstavničke grupe i proširenja FairCoop-a na lokalnom nivou. Oni nezavisno razvijaju i proširuju regionalnu mrežu promoviranjem FairCoop alata prema prodavnicama i pojedincima, dajući tehničku podršku, održavajući prezentacije, odgovarajući na sve vrste pitanja ili čak pokrećući pojedine projekte lokalnog čvora na okosnici FairCoop principa. Lokalni Čvorovi mogu primiti manje iznose za finansiranje da pokriju osnovne troškove sa namjerom da postanu samo-održive protekom vremena.

Da se uspostavi lokalni čvor, potrebno je najmanje 3 aktivna učesnika. Potičemo svakoga ko je zainteresovan da stupi u direktan kontakt s ljudima iz lokalnog čvora vaše regije. Mapu Lokalnih Čvorova možete pogledati, sa više detalja, [ovdje:](http://map.fairplayground.info/). Ako ne možete pronaći lokalni čvor u vašoj blizini, mi ćemo vam rado pomoći i podržati u uspostavljanju istog.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Više Detalja Možete Pronaći U Vodiču Za Lokalne Čvorove, Ovdje</a>