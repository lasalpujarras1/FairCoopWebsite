---
title: 'Мрежа и организација'
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

FairCoop je organizovan putem otvorene saradnje. Svako sa svojim individualnim veštinama može da učestvuje u razvoju ovog pokreta.

Budući da radimo na globalnom nivou, kako bi podržali decentralizaciju i lokalne ekonomije, naš pristup podrazumeva glokalni (globalni i lokalni) organizacioni model.

## Globalne radne oblasti
se uglavnom odnose na rad putem interneta, sa ciljem da se strukturiraju, organizuju i razviju informacije i tehnologije potrebne globalnoj mreži. Svi zadaci su organizovani putem otvorene kolaborativne platforme (OCP). Učesnici mogu da dobrovoljno izvršavaju radne zadatke, ili da potražuju naknadu u FairCoin-ima.

Naše globalne radne oblasti su:
* Opšti menadžment
* Komunikacija
* Cirkularna ekonomija
* Tehnološki razvoj
* Dobrodošlica-Edukacija-Podrška


[Više O Strukturi Rada U Globalnim Oblastima](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Rad lokalnih petlji
se odnosi i na aktivnosti putem interneta, i rad na terenu lokalne petlje, kako bi se razvila mreža sa fokusom na individualni i rad od šireg značaja. Neke od oblasti rada u lokalnim petljama mogu da budu:
* Dinamizacija razvoja cirkularne ekonomije (podrška trgovcima)
* Promocija različitih resursa FairCoop-a
* Upravljanje menjačnicom - Point of Exchange (PoE)
* Promocija vrednosti i iskustva FairCoop-a unutar lokalnih praksi
* Pokretanje posebnih lokalnih projekata u okviru FairCoop ekosistema

Svaka globalna radna oblast i svaka lokalna petlja imaju autonomiju u unutrašnjem donošenju odluka, putem zasedanja, a radi ispunjenja zadataka i ciljeva na najbolji način. Zbog toga, centralizovana odobrenja nisu potrebna. Ipak, o ključnim stvarima, koje su od interesa za celu zajednicu, odlučuje se na generalnim globalnim skupštinama, konsenzusom.

Sve globalne radne oblasti i lokalne petlje imaju razne grupe za razgovore, kao i podgrupe za svakodnevnu komunikaciju i koordinaciju.

Ukoliko ste zainteresovani za učešće, molimo vas priključite se našoj [grupi dobrodošlice](https://t.me/askfaircoop) i direktno nas kontaktirajte. Posle vas možemo voditi do mesta koje će najviše odgovarati vašim interesima i namerama.

## Mreža lokalnih petlji
Lokalne petlje su regionalne reprezentativne grupe koje predstavljaju širenje FairCoop-a. One nezavisno razvijaju mrežu putem promocije FairCoop resursa, prodavnicama i pojedincima, dajući im tehničku podršku, održavajući prezentacije, odgovarajući na sva pitanja, ili čak pokretanjem individualnog projekta u okvirima FairCoop principa. Lokalne petlje možemo delimično da finansiramo kako bi pokrile svoje osnovne potrebe, sa ciljem da vremenom postanu samoodržive.

Za osnivanje lokalne petlje potrebna su najmanje tri aktivna učesnika. Podržavamo svakoga ko je zainteresovan da stupi u direktan kontakt sa ljudima iz lokalne petlje iz njegovog regiona. Mapu lokalnih petlji, sa više detalja, [možete pronaći ovde: ](http://map.fairplayground.info/). Ukoliko ne možete da pronađete lokalnu petlju u svom regionu, rado ćemo vam pomoći i podržati vas u osnivanju vaše sopstvene.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Više Detalja U Priručniku Za Lokalne Petlje.</a>