---
title: 'Network û Rêxistinê'
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

The FairCoop communities organize themselves through open cooperativism. Everybody is encouraged to participate with their individual skills and help the movement grow.

As we are working globally to support decentralization and the local economy our approach is to follow a glocal (global and local level combined) organization model:

## Global Work Area (Open Coop Work)
refers mainly to online work, aiming to structure, organize, and develop information and technology for the global network. All tasks are organized in our open collaborative platform (OCP). Participants can either volunteer their hours or claim a remuneration in FairCoin for the work needed to be done.

The working areas are:
* Common Management
* Communication
* Circular Economy
* Technology Development
* Welcome-Education-Support

[More About The Global Work Structures Here](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Local Work Area
refers to both to online and offline activities of the corresponding local node to develop the physical network on the ground with its focus on individual regional work. Some Local work areas could be:

* Dynamization of circular economy (merchants support)
* Promotion of different FairCoop Tools
* Managing a Point of Exchange (PoE)
* Promote FairCoop values and practices to local projects
* Construction of locally specific projects within the Faircoop ecosystem

Each Global Work Area and Local Node have internal decision autonomy through assemblies to fulfill their tasks and achieve their goals in its best way. Therefore, centralized permissions are not needed. However, key issues, which concern the whole community, are decided in the general global assembly, as always by consensus.

All global work areas and local nodes areas have diverse chat groups and subareas for daily communication and coordination.

If you are interested in participating, please join our welcome group and get in direct contact with us. From there, we can lead you to the place which fits best to your interests and intentions.

[More Details Can Be Found In The Local Nodes Guide Here.](https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D)

## Local Nodes Network
Local Nodes are the regional representation groups and expansion of FairCoop on the local level. They develop and broaden the regional network independently by promoting FairCoop tools to shops and individuals, giving technical support, holding presentations, answering all kinds of questions or even by starting their individual local nodes project within the framework of the FairCoop principles. Local Nodes can receive small fundings to cover basic costs with the intention of being self-sufficient over time.

To establish a local node, at least 3 active participants are neccesary. We encourage everybody who is interested to get in direct contact with the people in the local node in your region. Further details you can here in local nodes map. If you cannot find a local node in your area we are happy to help and support you in setting one up.

[More Details Can Be Found In The Local Nodes Guide Here](https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D)