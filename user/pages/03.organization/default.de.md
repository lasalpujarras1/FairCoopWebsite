---
title: Organisation
media_order: organization.864f291a.jpg
taxonomy:
    category:
        - test
    tag:
        - tag
        - tags
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Die FairCoop-Gemeinschaft organisiert sich selbst durch ein offenes Kooperationswesen. Jeder wird ermutigt, sich mit seinen individuellen Fähigkeiten einzubringen und somit die Bewegung wachsen zu lassen. Da wir global arbeiten, um eine Dezentralisierung zu unterstützen, verfolgen wir ein glokales (global und lokal kombiniert) Organisationsmodelle.

## Globale Organisation
Die 'Global Work Areas'(Open Coop Work) beziehen sich hauptsächlich auf Online-Arbeit mit dem Ziel, Informationen, Technologien und Kontakte für das globale Netzwerk zu strukturieren, zu organisieren und weiterzuentwickeln. Alle Aufgaben werden in unserer offenen kollaborativen Plattform (OCP) verwaltet. Die Teilnehmer können sich entweder freiwillig mit ihrer Arbeit engagieren oder eine Vergütung in FairCoin bekommen.

Unsere globalen Arbeitsbereiche sind:
* Allgemeines Management
* Kommunikation
* Zirkuläre Wirtschaft
* Technologieentwicklung
* Willkommen-Bildung-Support

[Mehr Über Die Strukturen Der Globalen Arbeitsbereiche Gibt Es Hier](https://www.mindomo.com/mindmap/free-mind-map-ffa5505555b54101960e3138e13b5e89)

## Lokale Arbeitsbereiche
Die Arbeit auf lokaler Ebene bezieht sich sowohl auf Online- als auch auf Offline-Aktivitäten der jeweiligen lokalen Knoten, um das physische Netzwerk vor Ort mit Fokus auf die individuelle und regionale Arbeit weiterzuentwickeln. Einige lokale Arbeitsbereiche können sein:
* Dynamisierung der zirkulären Wirtschaft (Händlerunterstützung)
* Förderung verschiedener FairCoop-Tools
* Verwaltung eines FairCoin PoE (Point-of-Exchange)
* Förderung der FairCoop-Werte und -Praktiken bei lokalen Projekten * Aufbau von lokal-spezifischen Projekten innerhalb des FairCoop Ökosystems

Jeder globale Arbeitsbereich und jeder lokale Knotenpunkt verfügt über interne Entscheidungsautonomie, um die selbstdefinierten Aufgaben zu erfüllen und die Ziele optimal zu erreichen. Zentralisierte Berechtigungen sind daher nicht erforderlich. Schlüsselfragen, die die gesamte Gemeinschaft betreffen, werden jedoch in der globalen Versammlung entschieden, wie immer im Konsens.

Alle globalen und lokalen Arbeitsbereiche haben außerdem verschiedene Chatgruppen und Teilbereiche für die tägliche Kommunikation und Koordination.

Wenn du Interesse hast teilzunehmen, dann tritt unserer [Willkommensgruppe](https://t.me/askfaircoop) bei und nimm direkt mit uns Kontakt auf. Von dort aus können wir dich an den Ort führen, der deinen Interessen und Absichten am besten entspricht.

Netzwerk lokaler Knoten
Lokale Knotenpunkte sind regionale Gruppen, die FairCoop auf lokaler Ebene repräsentieren. Sie entwickeln und vergrößern das regionale Netzwerk selbständig, indem sie die FairCoop-Werkzeuge bei Geschäften und Einzelpersonen bekannt machen, technische Unterstützung leisten, Präsentationen halten, Fragen aller Art beantworten oder sogar im Rahmen der FairCoop-Prinzipien ihr eigenes lokales Knotenpunktprojekt starten. Lokale Knotenpunkte können kleine finanzielle Förderungen zur Deckung der Grundkosten erhalten, mit der Intention sich auf langfristig Sicht selbst tragen zu können.

Wir ermutigen alle, die Interesse haben an einem lokalen Knoten mitzumachen, direkten Kontakt mit den Menschen dort aufzunehmen. Weitere Details dazu gibt es auf der globalen Karte der lokalen Knoten [hier.](http://map.fairplayground.info/). Wenn du in deiner Nähe keinen lokalen Knoten finden kannst, helfen wir gerne bei einer Neugründung. Um einen lokalen Knoten einzurichten, sind mindestens 3 aktive Teilnehmer erforderlich.

<a class="block-link" href="https://git.fairkom.net/faircoop/MediaCommunication/wikis/How-to-create-a-Local-Node-%5Ben%5D">Mehr Informationen Gibt Es In Unserem Handbuch Zum Thema.</a>