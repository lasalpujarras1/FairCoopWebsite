---
title: 'Ferramentas  FairCoop'
media_order: tools.jpg
menu: Ferramentas
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>A FairCoin é uma moeda digital baseada em uma rede descentralizada, peer-to-peer, protegida por uma encriptação robusta. É uma ferramenta poderosa para mudar o mundo através da independência de bancos centrais, instituições financeiras e governos. A moeda é, portanto, uma ferramenta chave no novo sistema econômico. Em comparação a outras criptomoedas, como a Bitcoin, o valor da FairCoin é estável e ajustado frequentemente pela comunidade, mediante consenso.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Ir para FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>O FairPay é um sistema com tecnologia sem fio que torna pagamentos com FairCoin tão simples, que é como se eles fossem com cartões de crédito convencionais, tanto para os comerciantes quanto para os consumidores. A etiqueta de NFC contém uma ID única que identifica o usuário do cartão e se comunica diretamente com o painel do app vinculado. Através deste painel, os comerciantes podem ainda trocar FairCoin diretamente pela moeda local, se necessário.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Ir para FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>O FairFund tem três fundos principais: o Fundo Global Sul, o Fundo de Recursos Comuns e o Fundo de Infraestrutura Tecnológica. O capital em FairCoin armazenado nesses fundos tem como objetivo financiar projetos futuros que estejam em harmonia com o objetivo de cada um e apoiar a economia circular mais ampla da FairCoin. Devido ao crescimento estável do valor da FairCoin como moeda, o poder econômico dos fundos também aumenta constantemente. Assim, somos cada vez mais capazes de financiar projetos cada vez maiores em direção a nossos objetivos comuns.
</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Ir para FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org mostra um mapa virtual e diretório de locais físicos que aceitam pagamento com FairCoin. O site é útil para que as pessoas encontrem facilmente estabelecimentos onde podem usar FairCoin para adquirir produtos e serviços, ao mesmo tempo que apoiam a economia circular.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">GIr para useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net é uma maneira fácil de comprar FairCoin através de diferentes moedas convencionais, como o euro, dólares americanos, libras esterlinas e até reais, além de várias outras. Os métodos de pagamento são os mesmos usados normalmente, como transferência bancária, cartão de crédito ou débito e até mesmo a troca por dinheiro vivo em lugares físicos, como os nodos locais. O valor oficial da FairCoin é estável, definido em assembleia e frequentemente ajustado de acordo com diversos parâmetros de preço.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Ir para getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>A FairSaving é o serviço de poupança da FairCoin, oferecido pela FairCoop através da plataforma GetFairCoin. Está disponível para pessoas que queiram usar a nossa infraestrutura para guardar FairCoin sob todas as devidas medidas de segurança. É recomendável para indivíduos que não têm um computador próprio ou para aqueles que não se sentem confiantes o suficiente com seus conhecimentos de informática para assumir a responsabilidade de cuidar da própria carteira digital.
</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Ir para FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>Como plataforma colaborativa aberta, a OCP é principal ferramenta organizacional que oferecemos a indivíduos, coletivos e coordenadores de projetos da FreedomCoop, Bank of the Commons e, no futuro, muitos outros projetos cooperativados. Ela também pode ser usada para gerenciar projetos coletivos abertos e o trabalho em equipe: criar tarefas, contabilizar o tempo investido por cada membro para a distribuição de renda ou gerenciar carteiras da FairCoin online para aceitar e fazer pagamentos.
</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Ir para OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>A FreedomCoop é uma Sociedade Cooperativa Europeia (SCE) que oferece um conjunto de ferramentas para autogestão, trabalho autônomo, autonomia econômica e desobediência financeira. Os membros autônomos podem, por exemplo, entregar faturas à FreedomCoop e receber o pagamento em contas virtuais individuais. Não é necessário fornecer documentos de identidade ou nada semelhante. Todas as taxas de adesão são pagas em FairCoin e, como os lucros da FreedomCoop são redistribuídos para os Nodos Locais, eles são cruciais para financiar e dinamizar a criação de mais núcleos.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Ir para FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>O FairMarket é o mercado online da FairCoop, onde todos os participantes podem oferecer seus produtos e serviços à comunidade. Ele conecta indivíduos que estão alinhados a mercados alternativos, moedas sociais, comércio justo e aos princípios gerais da FairCoop. Todos os produtos e serviços são pagos em FairCoin. Além de aumentar a circulação e o uso da moeda, o site ajuda a criar alternativas para uma economia mais igualitária.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Ir para FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>O Bank of the Commons (BotC) é uma iniciativa colaborativa aberta com o objetivo de transformar o sistema bancário, os pagamentos e as moedas para apoiar a economia de cooperativas e movimentos sociais, tanto em nível global como local. O BotC adotou a FairCoin como moeda social global e sua tecnologia de blockchain como parte do desenvolvimento e adoção de estruturas financeiras descentralizadas para a coletividade. O BotC tem links diretos com o kit de ferramentas da FairCoop a fim de ampliar ao máximo possível o grupo de usuários em potencial.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Ir para BankoftheCommons</a>

</div>

</div>