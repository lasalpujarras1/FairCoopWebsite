---
title: 'Amûrên FairCoop'
media_order: tools.jpg
menu: Amûr
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin: Diravê Adîl</h2>
<p>FairCoin diravek dijîtale li ser bingeha ne navendîtî ya bi torên hevcûre ku bi şifrekirinek pir bihêz tê parastine . Ew amêrek pir bihêze ji bo guhertina cîhanê bi serbixwe ji banqên navendî, dezgehên fînansê û hukumetan. Bi vî awayî ew amêrek kilîle di sîstema aborî ya nû de. Beramber bi diravên krîpto yên din, weku Bitcoin, nirxê wê sabîte û bi berdewamî bi lihevkirinê ji aliyê civakê ve tê eyarkirin.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Herin FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>Ev sîstemek ya karta NFC ye ku ji bo miştirî û bazirgan pereyê FairCoin bidin bi rêyek herî hesanî ya kredî kart. Lezaqoka NFC nasnameyek bê hevşêwe digre nava xwe ku nasandina bikarhînerê kartê dike û bi raste rast peywendiyê li gel lînka Panela bikaranînî (app) yê dike. Bi rêya vê panelê, heta bazirgan jî xweyî îhtimala wê çendêne dikarin FairCoinên xwe veguherînin bi Euro ger neçarî be.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Herin FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Sê Fonên Adîl yên sereke hatine çêkirin, Fona Başur ya Cîhanî, Fona Giştî û Fona Avahîsaziya Teknîkî. Faircoinên depokirî yên di van fonan de bi armanca fînansekirina projeyên pêşerojêne ku bi hevahengiya her serkeftina her fonê û ji bo piştgiriya aborîyek çemberî ya mezintir ya FairCoinê ye. Bi sedema mezinbuna nirxê sabît yê FairCoinê weku dirav, herwuha hêza ekonomîk ya fonê jî wusa mezin dibe. Bi vî awayî em dikarin pitir fonê bidin û projeyên mezintir ji bo serkeftinên xwe yên giştî diyar bikin.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Herin FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org nexşeyek rastî ye û rêveberiya pejirandina hemî cûrên qadên fizîkî yên rêbaza peredayîna FairCoinê ye. Ev bi taybet sûdmende ji bo bikarhîneran ku bi hêsanî herêman bibînin ji bo ku FairCoinên xwe bimezêxin di cîhanek rasteqîn ya berhem û xizmetguzariyan de, û di heman demê de piştgiriya aboriya bazinî jî bikin.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Herin useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net rêyek hesanî ye ji bo kirrîna FairCoin bi rêya diravên neteweyên cuda yên wek EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, hwd bi rêbazên peredayînên giştî yên weke transfera bi têlan, kredît, kartên banqan û heta dikarin pere pêşîn bikarbînin, bi rêya cihên fizîkî (NH). Nirxê van FairCoinên fermî sabîte, ji aliyê meclîsan ve tê bi navkirin û li gora parametreyên nirxên ciyewaz tê lihevanîn.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Herin getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving ango teserufa adîl, xizmetek ya teserûfkirina FairCoinê ye ku bi rêya platforma getFairCoin tê amadekirin . Ji bo wan kesan tê pêşniyarkirin ku dixwazin avahîsaziyên me bikarbînin ji bo ku FairCoinên xwe bidin bin hemî pîvanên ewlekariyên guncaw. Ji kesên ku komputera wan ya kesî nîne tê pêşniyarkirin, anjî ji bo wan kesên ku bi kêrhatinên xwe yên komputerî nebawerin berpirsyartiya cûzdana kopê bigre ser xwe.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Herin FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP amêrek kilîl ya rêxistinkirinê ye ku em pêşniyarê kesan, kolektîvan, û rêveberên-kor dînatororên projeyan dikin di nava sîstema FreedomCoopê, Banqa Hevbeşan û gelek projeyên koperatîvên cudatir yên di pêşerojê de dikin. Herwuha dikare bê bikaranîn ji bo, mînak, rêvebirina projeyên koperatîvên cuda û çêkirina hêza pêkanîna xebatên tîmên wê, demê ku bi xebata vê ve bêt derbazkirin dibe bingeha dabeşkirina dahatê ji bo her endamekê, anjî bi çêkirina cûzdana FairCoinê ya ser internetê re dayînên xwe çêbikî û bipejirînî.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Herin OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>Ev koperatîv Civaka Koperatîvên Ewrupî ye (SCE) ku amêran pêşniyarê rêveberiyên xweser, pîşeyên serbest, xweseriya aborî û bê îtiatiya fînansî dike. Endamên pîşeyên serbest dikarin, mînak, bihayên xwe bidin FreedomCoopê û diravên xwe ji hesabê xwe yê rastîn werbigrin. Nasnameya kesî anjî belgeyên din nayên xwestin. Hemî xerçên endamtiyê di FairCoinê de tên dayîn. Sûdên FreedomCoopê ewe ku ji nûvekirine bo Nodên Herêmî û bi vî awayî girînge bo fînansê û dînamîkkirina afirandina wê.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Herin FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>Ev cihek marketa ser internetê ya FairCoopê ye, ku her beşdar dikare berhem û xizmetguzariyên xwe pêşkêşî civakê bike. Ew bi gelek cûran di nava peywendiyên piralî de ye bi kesên di marketên alternatîv de, diravên civakî, bazirganiya adîl û prensîbên FairCoop yên giştî de. Hemî xizmetguzarî û berhem di FairCoinê de têne dayîn. Ev zêdebûna sabîtî/zivirîna dirav zêde dike û alîkarî dike ku alternatîvan ava bike ji bo aborîyek yeksan ya zêdetir.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Herin FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Banqa Hevbeşan (Bank of the Commons-BotC) însiyatîfek koperatîvî ye ku armanca wê transformekirina banqevanî, pere dayîn û diravane bi mebesta piştgirîkirina aborîya di koperatîv û bizavên civakî de tên bikaranîn di astên cîhanî û herêmî de. Ev banq FairCoin weku diravek civakî yê cîhanî yê stratejîk dipejirîne û zincîre teknolijiya wê jî weku beşek yê pêşketin û pejirandina avahîsaziya ne navendîbûna fînansa Hevbeş dipejirîne. Ev banq ne raste rast girêdayîye bi amêrên FairConê ve bo firehkirina bikarhînerên di qadan de hingî kû îhtimal heye bi ser bixe.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Herin BankoftheCommons</a>

</div>

</div>