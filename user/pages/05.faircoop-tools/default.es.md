---
title: 'Herramientas FairCoop'
media_order: tools.jpg
menu: Herramientas
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin es una moneda digital basada en una red entre pares (p2p-peer to peer), descentralizada y protegida con una fuerte encriptación. Es una poderosa herramienta para cambiar el mundo, que nos permite independizarnos de los bancos centrales, las instituciones financieras y los gobiernos. Es, por tanto, una herramienta clave en el nuevo sistema económico. Comparada con otras criptomonedas, como Bitcoin, su valor es estable y se somete a ajustes regulares mediante el consenso de la comunidad.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Ir a FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay es un sistema de tarjeta NFC para que pagar con FairCoin sea tan sencillo para clientes y comercios como hacerlo con una tarjeta de crédito. La etiqueta NFC contiene un ID único que identifica a la usuaria de la tarjeta y que se comunica directamente con el panel de la App a la que está vinculada. A través de dicho panel, los comerciantes tienen incluso la posibilidad de cambiar directamente los FairCoin que reciben por euros, en caso necesario.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Ir a FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>Existen tres grandes FairFunds: el Fondo Global Sur, el Fondo de los Commons y el Fondo de Infraestructura Tecnológica. Los FairCoin almacenados en estos fondos tienen por objeto financiar futuros proyectos que estén en armonía con las metas de cada fondo y apoyar, a mayor escala, la economía circular de FairCoin. Debido al crecimiento estable del valor de FairCoin como moneda, el poder económico de los fondos también está creciendo constantemente. Así, cada vez somos capaces de financiar más y mayores proyectos que estén orientados hacia nuestros objetivos comunes.</p>

<a class="btn btn-tools" href="https://2017.fair.coop/fairfunds/" target="blank">Ir a FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org es un mapa y un directorio virtual de espacios físicos de todo tipo que aceptan FairCoin como método de pago. Esto es especialmente útil para que las usuarias encuentren con facilidad lugares en los que gastar sus FairCoin a cambio de bienes y servicios del mundo real, al tiempo que apoyan la economía circular.</p>
<a class="btn btn-tools"href="https://use.fair-coin.org/" target="blank">Ir a useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin.net es una forma fácil de comprar FairCoin a través de diferentes monedas nacionales, como EUR, USD, SYP, CNY, RUB, GBP, INR, NGN, BRL, MXN, etc. con métodos de pago comúnmente utilizados, como transferencia bancaria, tarjeta de crédito o débito o incluso usando efectivo, en un lugar físico (nodos locales). Este precio oficial de FairCoin es estable, definido por la asamblea y ajustado con frecuencia en función de diferentes parámetros de precio.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Ir a getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving es un servicio de ahorros de FairCoin que FairCoop proporciona a través de la plataforma getFairCoin. Se ofrece a aquellos que quieren utilizar nuestra infraestructura para mantener sus FairCoin bajo todas las medidas de seguridad apropiadas. Se recomienda para las personas que no tienen un ordenador propio o para aquellas que no se sienten lo suficientemente confiadas con sus habilidades informáticas como para asumir la responsabilidad de cuidar su billetera Coop.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Ir a FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP es la herramienta organizativa clave que ofrecemos a personas, colectivos y coordinadoras de proyectos dentro de Freedom Coop, de Bank of the Commons y de muchas otras iniciativas cooperativas que vendrán en el futuro. También se puede utilizar, por ejemplo, para gestionar proyectos colectivos abiertos y sus equipos de trabajo, permitiendo crear grupos de tareas, contabilizar el tiempo empleado por cada miembro para distribuir los ingresos o gestionar una billetera FairCoin online para aceptar y realizar pagos.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Ir a OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>Freedom Coop es una Sociedad Cooperativa Europea (SCE) que ofrece un conjunto de herramientas para la autogestión, el autoempleo, la autonomía económica y la desobediencia financiera. Las miembros autónomas pueden, por ejemplo, entregar sus facturas a FreedomCoop y recibir su dinero en su cuenta virtual individual. No se requieren identificaciones personales u otros documentos. Todas las cuotas de membresía se pagan en FairCoin. Los beneficios de FreedomCoop se redistribuyen a los nodos locales y son, por tanto, cruciales para financiar y dinamizar su creación.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Ir a FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket es el mercado online de FairCoop, en el que todas las participantes pueden ofrecer sus productos y servicios a la comunidad. Interconecta a las personas que están en sintonía con los mercados alternativos, las monedas sociales, el comercio justo y los principios generales de FairCoop. Todos los productos y servicios se pagan en FairCoin. Esto aumenta la usabilidad/circulación de la moneda y ayuda a construir alternativas para una economía más equitativa.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Ir a FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons (BotC) es una iniciativa cooperativa abierta cuyo objetivo es transformar la banca, los pagos y las monedas para apoyar la economía de los movimientos cooperativos y sociales a nivel global y local. BotC ha adoptado FairCoin, como moneda social global estratégica, y su tecnología de cadena de bloques como parte del desarrollo y la adopción de estructuras financieras descentralizadas para los commons. BotC está indirectamente ligado al conjunto de herramientas de FairCoop para ampliar su campo potencial de usuarias tanto como sea posible.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Ir a BankoftheCommons</a>

</div>

</div>
