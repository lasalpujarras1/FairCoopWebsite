---
title: Faircoop-Werkzeuge
media_order: tools.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<div class="row col-md-12">
<div class="tool-item col-md-6">
<h2>FairCoin</h2>
<p>FairCoin ist eine digitale Währung, die auf einem dezentralen peer-to-peer Netzwerk basiert, welches durch eine starke Verschlüsselung gesichert ist. Es ist ein mächtiges Werkzeug um die Welt durch die Unabhängigkeit von Zentralbanken, Finanzinstitutionen und Regierungen zu ändern. Es ist daher ein Schlüsselinstrument im neuen Wirtschaftssystem. Im Vergleich zu anderen Kryptowährungen, wie Bitcoin, ist der Wert stabil und wird regelmäßig durch Konsensentscheidungen der Community angepasst.</p>

<a class="btn btn-tools"href="https://fair-coin.org/" target="blank">Link FairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairPay</h2>
<p>FairPay ist ein NFC-System für Kunden und Händler um FairCoin Bezahlungen so einfach zu gestalten wie mit einer Kreditkarte. Der NFC-Sticker beinhaltet eine einzigartige ID, die den Benutzer der Karte identifiziert und direkt mit dem verbundenen App Panel kommuniziert. Über diesen Panel haben die Händler sogar die Möglichkeit, die erhaltenen FairCoins bei Bedarf direkt in Euro umzutauschen.</p>
<a class="btn btn-tools" href="http://fairpay.fair.coop/" target="blank">Link FairPay</a>

</div>

<div class="tool-item col-md-6">
<h2>FairFund</h2>
<p>UseFairCoin ist eine virtuelle Landkarte bzw. ein Verzeichnis physischer Räume aller Art, die FairCoin als Zahlungsmethode akzeptieren. Dies ist besonders nützlich um Standorte leichter zu finden, an denen man FairCoins für reale Produkte und Dienstleistungen ausgeben und gleichzeitig die zirkuläre Wirtschaft unterstützen kann.</p>

<a class="btn btn-tools"href="https://2017.fair.coop/fairfunds/" target="blank">Link FairFunds</a>
</div>

<div class="tool-item col-md-6">
<h2>useFairCoin</h2>
<p>Use.fair-coin.org is a virtual map and directory of physical spaces of all kinds accepting FairCoins as a payment method. This is especially useful for users to easily find locations to spend FairCoins for real-world products and services, and to support the circular economy at the same time.</p>
<a class="btn btn-tools" href="https://use.fair-coin.org/" target="blank">Link useFairCoin</a>

</div>


<div class="tool-item col-md-6">
<h2>getFairCoin</h2>
<p>GetFairCoin ist eine einfache Methode, FairCoin über verschiedene nationale Währungen (wie EUR, USD, SYP, CNY, RUB, GBP, GBP, INR, NGN, BRL, MXN, etc.) mit gebräuchlichen Zahlungsmethoden wie Banküberweisung, Kredit- oder Debitkarte oder sogar mit Bargeld an lokalen Knoten zu kaufen. Der offizielle getFairCoin-Preis ist stabil, wird durch die globale Versammlung definiert und aufgrund diverser Preisparameter regelmäßig angepasst.</p>

<a class="btn btn-tools"href="https://getfaircoin.net/" target="blank">Link getFairCoin</a>
</div>

<div class="tool-item col-md-6">
<h2>FairSaving</h2>
<p>FairSaving ist ein FairCoin Sparservice, den FairCoop über die getFairCoin Plattform anbietet. Es ist ein Angebot an diejenigen, die unsere Infrastruktur nutzen wollen, um ihre FairCoins unter allen geeigneten Sicherheitsvorkehrungen zu verwahren. Es empfiehlt sich beispielsweise für Personen ohne eigenen Computer oder für diejenigen, die sich mit ihren Computerkenntnissen nicht sicher genug fühlen, die Verantwortung für ihre digitale Geldbörse selbst zu übernehmen.</p>
<a class="btn btn-tools" href="https://2017.fair.coop/fairsaving-2/" target="blank">Link FairSaving</a>

</div>


<div class="tool-item col-md-6">
<h2>Open Collaborative Platform</h2>
<p>OCP ist ein entscheidendes Organisationsinstrument, das wir Einzelpersonen und Kollektiven zur Koordinatoren und Verwaltung anbieten. So nutzen auch Freedom Coop, Bank of the Commons und unsere lokalen Knoten diese freie Plattform. Es kann dazu verwendet werden um die Teamarbeit in offenen Projekte finanziell zu verwalten, indem die Arbeitszeit jedes Teilnehmers als Einkommensgrundlage verbucht wird und das Geld später entsprechend verteilt wird. OCP kann auch einfach als Online-Geldbörse verwendet werden um seine FairCoins zu verwalten und Zahlungen abzuwickeln.</p>

<a class="btn btn-tools"href="https://ocp.freedomcoop.eu/" target="blank">Link OCP</a>
</div>

<div class="tool-item col-md-6">
<h2>FreedomCoop</h2>
<p>FreedomCoop ist eine Europäische Genossenschaft (SCE), die ein Instrumentarium für Selbstverwaltung, Selbständigkeit, wirtschaftliche Autonomie und finanziellen Ungehorsam bietet. Selbstständige Mitglieder können z. B. ihre Rechnungen bei FreedomCoop einreichen und ihr Geld auf ihrem individuellen virtuellen Konto empfangen. Personalausweise oder andere Dokumente sind nicht erforderlich. Alle Mitgliedsbeiträge werden in FairCoin bezahlt. Die Gewinne von FreedomCoop werden an die lokalen Knotenpunkte umverteilt und sind somit entscheidend für deren Finanzierung und Dynamisierung.</p>
<a class="btn btn-tools" href="http://freedomcoop.eu/" target="blank">Link FreedomCoop</a>

</div>

<div class="tool-item col-md-6">
<h2>FairMarket</h2>
<p>FairMarket ist der Online-Marktplatz von FairCoop, auf dem alle Teilnehmer ihre Produkte und Dienstleistungen der Community anbieten können. Er verbindet Menschen, die mit alternativen Märkten, sozialen Währungen, fairem Handel und den allgemeinen FairCoop-Prinzipien im Einklang stehen. Alle Produkte und Dienstleistungen werden in FairCoin bezahlt. Dies erhöht die Nutzbarkeit und die Zirkulation der Währung, was dabei hilft eine gerechtere Wirtschaft zu schaffen.</p>

<a class="btn btn-tools"href="https://market.fair.coop/" target="blank">Link FairMarket</a>
</div>

<div class="tool-item col-md-6">
<h2>Bank of the Commons</h2>
<p>Bank of the Commons (BotC) ist eine offene Genossenschaftsinitiative, deren Ziel es ist, Banken, Zahlungen und Währungen zu transformieren, um die Wirtschaft der genossenschaftlichen und sozialen Bewegungen auf globaler und lokaler Ebene zu unterstützen. BotC hat FairCoin als strategische globale soziale Währung, sowie dessen Blockchain-Technologie zur Entwicklung und Einführung dezentraler Finanzstrukturen für die Commons ausgewäht. BotC ist indirekt mit dem FairCoop-Toolkit verbunden, um das potenzielle Nutzerfeld so weit wie möglich zu verweitern.</p>
<a class="btn btn-tools" href="http://bankofthecommons.coop/" target="blank">Link BankoftheCommons</a>

</div>

</div>
