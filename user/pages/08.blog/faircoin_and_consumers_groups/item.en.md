---
title: 'FairCoin and Consumer Groups'
media_order: faircoin_and_consumers_groups.jpg
summary:
    description: 'The consumer groups consist of groups of people who choose to organize their shopping basket themselves, without intermediaries. They choose the producers from whom they want to purchase food products, make agreements with them, and manage orders, payment and delivery methods.'
thumbnail_image:
    image: faircoin_and_consumers_groups.jpg
main_image:
    image: faircoin_and_consumers_groups.jpg
date: '10-02-2018 13:34'
taxonomy:
    category:
        - blog
    tag:
        - faircoin
        - 'consumer group'
        - comsumer
        - products
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

Self-management in general is one of the principles towards which FairCoop orients the design of its tools, with the aim of encouraging decentralised autonomy as much as possible in all possible places and forms. **Self-managed ecological consumer groups are an interesting option for healthy eating at a fair price, giving organic farmers back their dignity, fostering local economy and community relations, and caring for the environment.**

The consumer groups consist of groups of people who choose to organize their shopping basket themselves, without intermediaries. They choose the producers from whom they want to purchase food products, make agreements with them, and manage orders, payment and delivery methods.

What are the implications of this? Amongst others:

1. By getting rid of the middlemen, the markup on the price taken by them disappears, so that products can usually be more economical for consumers while producers also receive fairer prices.

2. The cause of organic farming - usually so belittled by the apparently baffling criteria applied by many of the official organic certifications -  is advanced. What is to be considered ecological or not, now becomes the exclusive decision of the consumer group. which takes into account not only the use in production of non-genetically-modified products or toxic chemicals, but also aspects such as geographical proximity and the working conditions of the producers. In this sense, we see the emergence of participative certifications in many cases that, although not "official", are nevertheless willingly accepted by those who have participated in their definition (what is truly organic according to them).

3. Consumers can get to know producers and their farms directly, thus creating a type of community relationship that does not exist at all when food is purchased at a supermarket. This trust relationship is arguably more effective than official certifications.

4. There is an environmental aspect, because consumer groups generally take much more conscious decisions than are involved in any environmental guarantee from an official body.

**Many of these groups carry out their transactions in fiat currency and some also combine it with local currencies. FairCoin is a good option as it can reach where the local currencies do not. In other words, it can be integrated into a circular economy of this kind in the same way as any social currency, but with added possibilities**

By incorporating FairCoin into these groups there is no need to be a "prosumer" within the circle, as is usually the case with mutual credit money, since it is exchangeable for fiat, provided that producers have registered themselves with FairCoop. In addition, they will not always need to change the FairCoin into euros, dollars, etc., since outside of the circle of the consumer group there is a much wider market in which participants can spend their FairCoin, in an area that not only exceeds the consumer group and their immediate local area, but which is international.

**Different local nodes can be coordinated to meet mutual needs that are beyond their reach and there is also a Circular Economy group for large suppliers on an international scale. Thus, from the most local to the most global, little by little, we are shaping a fairer, greener and more satisfying economy for all. No more and no less than a self-managed economy, that is: the economy we all want.**