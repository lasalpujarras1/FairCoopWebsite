---
title: 'Update Your ElectrumFair Wallet Now'
date: '17-02-2018 17:34'
taxonomy:
    category:
        - blog
    tag:
        - wallets
---

ElectrumFair is one of the wallets available for FairCoin. Its use is simple and, like any other FairCoin wallet, highly secure. **The new improved version 3.0.5 has just been released, and we recommend that you update the previous version.  Specifically if you are still using the 2.7.1 version of ElectrumFair or older, the update is mandatory.**

There was a bug found in January 2018 in all Electrum wallets that was fixed with version 2.8.4. Now, the update to 3.05 also incorporates the fix for a lesser issue. Lead FairCoin developer, Thomas König, explains: "It was an issue creating multi-signature transactions. The size, and thus the fees, were calculated incorrectly and so the transactions were not accepted by the network. The bug has been fixed in this release".

If you don't have your ElectrumFair wallet yet, this will also be the version you will have to install from now on, in case you want to try it. It is normal to have more than one wallet for FairCoin, as you can dedicate one to your daily expenses and another one for saving larger amounts; or have one for personal use and another for some commercial activity, for example. If you don't know ElectrumFair yet, these are some of its features:
* ** It's a light wallet:**
 This means that it hardly needs any resources on your device to operate, so it takes up very little space on your computer disk or on your mobile phone memory/SD card. 

* **Security:**
 Your private key consists of 12 words, called a "seed", which will be provided to you by the program itself and which you must print out and save (preferably in a different place from where you keep the device on which you install the wallet), because this is the code that will allow you to retrieve your ElectrumFair wallet and install it again, with your current balance and the record of your transactions, in case your computer or mobile phone is damaged, or anything happens that requires you to perform a recovery.

 Remember: It is very important that you keep these words in a safe place and don't lose them.

 From this master private key, as in other wallets, different addresses to receive FairCoin can be generated.

* **It can be installed on different devices:**
 ElectrumFair is available for Linux, Windows, MacOS and Android, and you can install it on multiple devices, if you wish. If you use the same seed for all of them, you will have a single synchronized wallet, and what you do in one will be reflected in the others. If you use different seeds, the corresponding wallets will be independent.

**You will find it very easy to update or install it. Simply download the appropriate file for your operating system from https://download.faircoin.world (link is external) and run it. **

* If you just want to update it, you only need to choose the "I already have a seed" option.
* If you want to install it from scratch, choose the "create new seed" option.

If you have any doubts when updating or installing ElectrumFair 3.0.5, please do not hesitate to contact us in our Telegram groups or ask at your nearest FairCoop Local Node.

Once installed or upgraded, you will have a lightweight, safe, and convenient FairCoin wallet. We hope you enjoy using it!