---
title: 'Las criptomonedas y la legalidad'
media_order: 'law_cryptos_faircoop1.png,law-cryptos-faircoin-china.jpg,law_cryptos_faircoop2.png'
date: '04-02-2018 02:33'
taxonomy:
    category:
        - blog
    tag:
        - faircoin
        - cryptocurrencies
        - laws
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

! Las criptomonedas en general son un tema del que se está hablando cada vez más en estos días, creando algunos puntos de discusión interesantes. Se dice mucho, tanto a favor como en contra, pero destacan algunos aspectos fundamentales que se abordan con mucha frecuencia. Así, en este artículo se intentará ofrecer una visión general de este intrincado tema, de las diversas implicaciones de la legalidad de las criptomonedas, lo que ello significa o implica, tanto para los gobiernos como para la gente. Primero deberíamos empezar abordando las criptomonedas en general, para pasar más tarde a nuestra situación específica en el caso de FairCoin.

Para empezar, una de las cosas más interesantes sobre las criptomonedas es su estatus, que actualmente podría describirse curiosamente como alegal. Esto, en el sentido de que muchas no están oficialmente legalizadas, pero tampoco son ilegales. Por tanto, ya existe una zona de indefinición en torno a su uso y a su realidad. Esto puede resultar amenazante, pero también excitante, ya que funciona como un método de educación política de bajo nivel.** La gente ya está superando la idea de que el Estado es el árbitro último de lo que se puede o no hacer; y el Estado, como institución, está perdiendo su recientemente establecido (históricamente hablando) monopolio de emisión y control de todas las herramientas financieras contables. **

**Cada vez más, los países hablan de regular las criptomonedas, lo que ya no es tan extremo como prohibirlas directamente.**. Este asunto surgió recientemente en el caso de Corea del Sur, uno de los mayores mercados de criptomonedas, y la declaración oficial sobre la prohibición de estas monedas y de sus casas de intercambio provocó una caída en el mercado de alcance general (junto con otros anuncios que se produjeron a principios de enero de 2018). Sin embargo, en un breve espacio de tiempo, se reunieron 200.000 firmas para protestar contra la propuesta de prohibición, salió a la luz que muchos empleados del Ministerio de Finanzas habían vendido criptomonedas poco antes de que se anunciara la prohibición y, finalmente, llegó la clara observación oficial de que probablemente era prácticamente imposible prohibir el comercio de estas monedas. Ahora, parece que está en marcha un sistema de regulación más práctico y que avanza de forma más lenta.

De hecho, el país que más ha experimentado en esta dirección, China, también ha visto algunos de los usos más frecuentes de las criptomonedas y ha instituido controles muy estrictos de identificación para los intercambios, entre otro tipo de controles. No obstante, a pesar de ello, el comercio no reconocido continúa a través de grupos de chat encriptados o breves reuniones personales. Gran parte del tráfico se ha ido a otros países cercanos, como Corea del Sur, y esto plantea algunas cuestiones políticas básicas. Por ejemplo, aunque para unirse a los intercambios chinos debe facilitarse la identificación, por otro lado se trata de intercambios que son casi intrínsecamente especulativos. Así, esto afectaría a las criptomonedas, pero no se centra tanto en los usos deseados específicamente para FairCoin -no se trata de comprar y vender en una bolsa digital para ganar dinero, sino de utilizarlo en proyectos y transacciones económicas prácticas, reales, justas y beneficiosas-. 

![](law-cryptos-faircoin-china.jpg)

	Además, si por el momento, por diversas razones políticas y culturales, muchos Estados europeos no están dispuestos a imitar los niveles extremos de control chino, muchos otros países simplemente no son capaces de gastar la gran cantidad de esfuerzo y conocimiento que requiere la regulación de criptomonedas. Por lo tanto, la cuestión de la legalidad o la ilegalidad plantea algunos límites funcionales interesantes: no todos los Estados son lo suficientemente fuertes, decididos y ricos como para prohibir o incluso canalizar de forma rigurosa el tráfico de estas monedas, aunque quisieran hacerlo. Esto también se debe a la realidad básica de que es muy difícil prohibir completamente las redes descentralizadas. De hecho, actualmente hay una especie de tendencia política por la que van apareciendo criptomonedas en lugares desestabilizados donde el Estado no es muy fuerte: Venezuela, Zimbabwe, Ucrania y Siria son ejemplos contemporáneos de esta tendencia objetiva. Los Estados débiles no tienen capacidad real de regular, y mucho menos de prohibir, las criptomonedas.

También hay dos cuestiones a tener en cuenta: muchas de las partes ricas de diferentes sociedades de todo el mundo han hecho pequeñas fortunas especulando con las criptomonedas y, por tanto, el "criptomundo" funciona como una especie de mercado bursátil no regulado, con todo tipo de potencial para el comercio de información privilegiada y las estafas con las que obtener dinero rápido (lo cual, de hecho, se ve cada vez más claro dadas las inmensas entradas de capital que han tenido lugar en 2017). También funciona como un método más sencillo para la evasión fiscal, de manera que parte del valor nominal del mercado de criptomonedas (por ejemplo, de Zcash y Monero, especialmente) es, de hecho, dinero del mercado negro que aparece de una forma cuantificable. Por tanto, por una parte, hay realmente cierto apoyo serio de la élite en torno a la continuación del "status quo" de las criptomonedas, lo que significa que los esfuerzos para regularlas serán lentos.

**También está el hecho de que, dada la naturaleza de la tecnología Blockchain, los esfuerzos para prohibirlas en un país lo único que conseguirán es que se vayan a otro. Sólo un esfuerzo global concertado podría prohibir verdaderamente las criptomonedas, pero en estos tiempos de crecientes y continuas tensiones imperialistas y fricciones regionales, eso parece algo impensable.** Si se prohiben en un país, los flujos de capital digital simplemente se irán a zonas más favorables y éstas se beneficiarán rápidamente de ello. Esto ya se está debatiendo, por ejemplo, en la empobrecida Bielorrusia, para tratar de convertir el país en un refugio IT para el dinero tecnológico. Este panorama podría concebirse algún día, en un futuro más pacífico, y aplicarse también a Rojava, para atraer el capital que tanto se necesita allí para la reconstrucción.

![](law_cryptos_faircoop2.png)

Los crecientes problemas económicos y la naturaleza del mercado especulativo de las propias monedas provocarán la precipitación general de su regulación, como pasa con otros activos o derivados financieros, y la exigencia de identificación para las usuarias de los portales de intercambio. Por tanto, habrá regulación, pero ésta no será contundente. Después de todo, el método correcto para gestionar las redes no es su prohibición total (algo que no funciona casi nunca), sino más bien su canalización. **Así, el esfuerzo consistirá realmente en "domesticar" las criptomonedas (o la minoría de monedas fuertes que sobrevivan a un posible desplome) y subordinarlas a los caprichos de la parte extrema de las clases altas, para que puedan utilizarlas como una herramienta constante para distintas formas de especulación y de evasión de impuestos -e intentando desplazar al 99% de la gente de este gran avance tecnológico, que realmente es una seria amenaza para todo el sistema estratificado de los Estados corporativos actuales-.**

	Como se ha visto recientemente, distintas partes se esforzarán por impedir el potencial intercambio por monedas fiat en los mercados de criptomonedas (ya sea por la conexión a un banco o a un servicio de tarjetas de crédito), de forma que sólo podrán sobrevivir las monedas que sean más atractivas para la élite actual (como, por ejemplo, las consideraciones de Putin sobre Ethereum o las recientes discusiones sobre el Criptorublo, la gran cantidad de dinero chino y de Hong Kong que se ha ido a BitCash, etc.). Finalmente, los Estados podrán su empeño en crear sus propias criptomonedas, que les sirvan como contrapartida a las fiat que ya tienen, y esto dará como resultado el monopolio del mercado.

Por lo tanto, **la verdadera pregunta no es la cuestión errónea y poco realista de si se van a prohibir totalmente o van a desaparecer** -esa visión refleja una comprensión anticuada del poder y la resistencia en nuestro mundo actual, que casi en todas partes funciona basándose en redes (no se trata de aniquilación o prohibición, sino de atenuación y neutralización)-. Y también, por lo general, estos debates se centran en monedas específicas (por ejemplo, la naturaleza problemática de Bitcoin), en lugar de hacerlo en el conjunto del mercado de criptomonedas.

Sin embargo, **la verdadera pregunta es: ¿en qué dirección se canalizará esta innovación de Blockchain? ¿Hacia el 1% que ocupa la cúspide de la élite mundial, hacia el capital financiero, o hacia la gran mayoría de las personas, que lo utilizarán para liberarse del opresivo sistema sociopolítico actual?**

Ésta es la verdadera línea divisoria en el mundo de las criptomonedas y lo que marca la diferencia en el funcionamiento, métodos y metas entre FairCoin y otras criptomonedas. 

Toda la discusión en torno a la legalización o regulación de las criptomonedas también pone de manifiesto una brecha en el pensamiento ideológico que hay detrás de la gran mayoría de las monedas, ya que éstas asumen que las preocupaciones económicas son las únicas que cuentan, como si la política no jugara un papel, como si el Estado no interviniera sistemáticamente para crear las condiciones de los mercados (o, recientemente, para rescatar a los bancos), y también como si no hubiera otra forma de comunidad humana que no fuera un agregado de especuladoras y consumidoras.

	La pregunta final en nuestro experimento de razonamiento o discusión hipotética pasa entonces a ser: si FairCoin tiene una orientación exclusivamente radical, para servir a la autonomía individual y colectiva y desvincularse del sistema económico oficial, ¿qué pasa si se prohibe? Esto supondría hacer un poderoso esfuerzo de canalización y que las únicas criptomonedas existentes o permitidas serían de naturaleza especulativa. Pero, inmediatamente, habría que preguntarse: ¿prohibidas dónde? Muchos países no estarían tan interesados ni encontrarían motivos para prohibirla, por lo que los fondos fluirían allí. Pero, en última instancia y a fin de cuentas, es cierto que si algún país propusiera la prohibición de FairCoin, desencadenaría una seria lucha de desobediencia financiera y política.

Otras monedas alternativas o experimentos sociales han sido prohibidas o saboteadas por los gobiernos cuando comienzan a funcionar demasiado bien (por ejemplo, la reciente moneda local en Argentina, Austria en el pasado o los famosos esquemas utópicos de Robert Owen). Pero estos esfuerzos no se digitalizaron, en primer lugar. Además, estos experimentos económicos no encajaban realmente en un esquema de resistencia más amplio, sino que tenían una concepción algo aislada de su propio funcionamiento. Tenían una visión del mundo economicista , como si fuera suficiente con tener un sistema económico fiable y no mereciese la pena considerar los aspectos políticos. Esto claramente fue un error.

Así pues, aprender de las experiencias pasadas y, en cierta medida, del patrimonio heredado, es precisamente la razón por la que FairCoin se interesa y se centra en conectarse con lugares como Rojava, Cataluña y Exarcheia, dado que, hoy en día, son lugares de resistencia y lucha política, donde FairCoin se conecta con otros movimientos de desobediencia y puede presentarse en su verdadero papel: no como una solución fetichista definitiva a todos los problemas de la vida, sino como una herramienta muy poderosa para el cambio, entre otras herramientas creadas por FairCoop. Y todo esto proviene de esfuerzos no estatales y desde la base, no de políticos reformistas, sino de resistencias cotidianas y conexiones personales.

Este es el curso lento pero constante que fundamentalmente cambia el mundo, a diferencia de los meteóricos ascensos de rápido éxito y duración efímera. **Es esto lo que promete seguridad para FairCoin y la hace diferente de otras criptomonedas: no se separa en su propio mundo economicista, tecnófilo o especulativo, sino que asume su importante rol como pieza de un mosaico de resistencias políticas, proyectos pluralistas y herramientas para la liberación, donde todas estamos trabajando para hacer realidad una visión colectiva de libertad y autonomía.**