---
title: 'Arte, cultura y activismo financiero: FairCoop se presenta en Londres en un evento con el aforo completo'
media_order: 'legislation_cryptocurrencies_faircoin_0 (1).jpg'
date: '26-01-2018 16:46'
taxonomy:
    category:
        - blog
    tag:
        - activism
        - culture
        - 'local nodes'
        - london
googletitle: 'Arte, cultura y activismo financiero, Londres'
googledesc: '"Arte, cultura y activismo financiero" fue el tema del MoneyLab, celebrado la Somerset House de Londres (Reino Unido) el pasado 20 de enero"'
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> "Arte, cultura y activismo financiero" fue el tema del [MoneyLab, celebrado la Somerset House de Londres (Reino Unido) el pasado 20 de enero](https://www.somersethouse.org.uk/whats-on/moneylab-art-culture-and-financial-activism), que agotó todas las entradas. Alrededor de 40 investigadores, artistas, diseñadores y empresarios culturales de todo el mundo presentaron un programa de **talleres, debates y experimentación artística a lo largo de todo un día, para explorar la relación entre el arte crítico, las tecnologías financieras descentralizadas, la organización colectiva y la desobediencia civil. **

> Anteriormente organizado por el Institute of Network Cultures de Amsterdam y ahora en su cuarta edición, MoneyLab es un programa de investigación crítica e intervención artística que explora las conexiones entre el arte contemporáneo, el activismo financiero y la cultura digital.

> **Como expuso su coordinadora Patricia de Vries durante la apertura del evento, al entrar en el décimo año de la crisis financiera global, es crucial distinguir "el vino viejo de las nuevas y elegantes botellas en las que se presenta".**. Según ella, las "soluciones" que se han presentado como alternativas no siempre son alternativas reales, como ocurre con muchas de las criptomonedas que existen actualmente: "Las élites de expertos en tecnología están acumulando Bitcoins y utilizándolos como mercancía; es un objeto especulativo, no una moneda. Hace falta tener dinero para montar la infraestructura necesaria para minarlos. Eso no es disruptivo, no es alternativo, es sólo un nuevo objeto de especulación. Es el mismo vino de antes, en una botella nueva".

**Usando esta metáfora, FairCoop y FairCoin pueden entenderse como vinos nuevos, refrescantes y deliciosos.**

Nuestro movimiento fue introducido en un taller titulado "La Cooperativa de la Tierra para la Justicia Económica".  Juan Cruz explicó que nuestra meta es construir otro sistema -abierto, cooperativo y amigable con el planeta- desde abajo; Jorge Saiz mostró cómo FairCoin aspira a convertirse en una moneda verde digital para un nuevo sistema económico global; y Enric Duran se unió a nosotras por vídeo conferencia para compartir la visión más amplia de la Revolución Integral. 

	Para celebrar la forma en que se distribuyó el primer lote de FairCoin, promovimos un "airdrop": se repartieron 500 FairCoin a partes iguales entre quienes se descargaron un monedero y nos enviaron su dirección dentro de las 24 horas siguientes a nuestro taller. Esos 500 FairCoin fueron donados por un miembro del grupo para la ocasión. Como resultado,  ya hay 16 personas más que pueden comprar bienes y servicios en el FairMarket, o ahorrar sus FairCoin para gastar en las tiendas que se vayan sumando al ecosistema local.

Aunque para la mayoría de las presentes ésta fue la primera vez que oían hablar de FairCoop, **también había unas cuantas personas que ya conocían nuestro ecosistema; y algunas, incluso, tenían ya un monedero de FairCoin**, como Katharine Vega, una artista e investigadora que conoció dicha criptomoneda en 2017. Como apasionada por el cambio climático, comenzó a preocuparse por la dimensión ética de las criptomonedas y por el impacto de la minería en el medio ambiente, lo que le llevó a buscar alternativas. "Estaba buscando en Internet información sobre criptomonedas éticas, cambio climático, ecología y cosas así", dice Katharine, explicando que le había deprimido la forma en que la gente hablaba del criptodinero. "Tenemos esta oportunidad de reorganizar por completo nuestra forma de hacer las cosas, pero parece que está siendo acaparada por las mismas las mismas viejas fuerzas de quienes quieren sacar provecho para sí mismas. Existe un potencial mayor y pienso que la mejor forma de aprovechar dicho potencial es tener modelos de trabajo con comunidades", concluyó Katharine, explicando que quiere usar FairCoin para apoyar a una comunidad en Vanuatu y que también quiere implicarse en el nodo local que se está creando en Londres. 

El evento concluyó con el lanzamiento de un libro - "MoneyLab Reader 2: Overcoming the Hype" - una colección de ensayos de artistas, académicos y activistas que exploran críticamente el arte, las finanzas y la tecnología, así como los territorios políticos y sociales creados a raíz del colapso financiero. FairCoin recibe una mención en "Postales del mundo del dinero descentralizado: Una historia en tres partes", un ensayo de Jaya Klara Brekke. Para descargar o pedir una copia física gratuita, haz [click aquí.](http://networkcultures.org/blog/publication/moneylab-reader-2-overcoming-the-hype/)