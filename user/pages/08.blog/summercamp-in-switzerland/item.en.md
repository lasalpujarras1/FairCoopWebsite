---
title: 'Summercamp in Switzerland'
media_order: 'summercamp2017-1152x400.png,img_20170825_194545.jpg,img_20170827_092049.jpg,img_20170827_143717.jpg,img_20170827_143654.jpg,img_20170825_154756.jpg,explainsummercampjurateam2017.jpg,sany0116.jpg,handsupsummercampjurateam2017.jpg'
date: '27-08-2017 06:57'
publish_date: '27-08-2017 06:57'
taxonomy:
    category:
        - blog
    tag:
        - education
        - camps
        - team
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

About 40 people from the fair.coop and FairCoin communities meet in the Jura Alps at a [Summercamp (link is external)](https://fair.coop/summercamps-faircoop-in-jura-switzerland-2017/) to review and plan the next steps for developing an ethical economic ecosystem utilizing blockchain technologies. Where do we want to go from now, after the successful migration to Proof-of-Cooperation? 

The first two weeks were dedicated to newcomers and another week to women only, who would like to get their hands on this new technology. Currently we are working for another two weeks on organizational and technical developments. We have identified five core areas:

* Welcome & Education
* General Management & Accounting
* Communication
* Technology
* Circular Economy

Here are the [meeting minutes](https://board.net/p/r.f2105d08d27f7b73f07d8cee1e5325be) of the intense discussions, which are being written collectively.

Ale from Barcelona is supporting the organization and communication team and enjoys the nice atmosphere: "In the summercamp we are designing a way of organic growth and creating thriving nodes in many regions".

Important decisions are prepared for the next general assembly meeting in September 2017. During the year, communication is being done in chat groups mainly. Thus an open meeting once a year is also good for the relations between those, who are active in various areas.
