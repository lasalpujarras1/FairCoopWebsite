---
title: 'Système économique'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

> FairCoop voit la révolution du système économique mondial comme le point d'entrée le plus prometteur pour provoquer et initier un changement collectif de la société.

L'un des objectifs prioritaires de FairCoop est de construire un nouveau système économique mondial basé sur la coopération, l'éthique, la solidarité et la justice dans nos relations économiques. Pour y parvenir, nous avons défini les causes majeures des problèmes du système économique actuel et les mécanismes sous- jacents qui causent un déséquilibre mondial du pouvoir et nous avons cherché comment les transformer pour le bien commun et global.  

Notre plan consiste à rétablir le plus haut niveau de justice économique mondiale possible en :
* Utilisant notre propre monnaie numérique FairCoin et en développant notre propre système économique totalement indépendant des banques centrales et des institutions
* Utilisant FairCoin pour ce qui a généralement été utilisé contre les régions du Sud du monde : les forces du marché (l'offre et la demande), les pirater et y insérer le virus coopératif.
* Utilisant le virus coopératif pour créer la confiance et inciter tout le monde à investir dans un système coopératif plutôt que d'un système compétitif volatil et spéculatif.
*Utilisant la stratégie de croissance stable pour augmenter la valeur de la monnaie en raison de l'offre limitée d'unités monétaires (caractère déflationniste) et l'augmentation de la demande de la monnaie.
* Utilisant la valeur accrue de la monnaie pour financer le développement de projets et d'outils qui sont en synergie avec les objectifs de FairCoop et qui emplifient le mouvement.
* Utilisant ces projets, ces outils et infrastructures pour faciliter l'utilisation commune de FairCoin pour d'autres produits et services qui sont alignés avec les principes FairCoop, et ainsi augmenter sa convivialité.
* Utilisant cette utilité croissante de FairCoin pour être globalement de plus en plus indépendant des monnaies fiduciaires et évoluer vers une économie circulaire.
* Utilisant l'économie circulaire, renforcée en incluant de plus en plus de produits et de services dans le monde entier, pour couvrir les besoins de base et intégrer naturellement plus d'utilisateurs (individus, groupes, réseaux) à FairCoin.
* Utilisant notre pouvoir collectif et financier provenant d'une participation et d'utilisateurs sans cesse croissants pour autonomiser et libérer de plus en plus de personnes de toute forme de répression économique ou politique.
* Utilisant les outils largement dispersés mais interconnectés, les réseaux humains, les services et les caractéristiques de ce système décentralisé indépendant comme base pour le changement social de toute la société mondiale, connue sous le nom de révolution intégrale.