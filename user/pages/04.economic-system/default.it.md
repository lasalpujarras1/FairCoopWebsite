---
title: 'Sistema economico'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop vede la rivoluzione del sistema globale economico cone l'azione più promettente per provocare e iniziare un cambio collettivo nella società.

Uno degli obiettivi prioritari di FairCoop è quello di costruire un nuovo sistema economico globale basato sulla cooperazione, sull'etica, sulla solidarietà e sulla giustizia delle nostre relazioni economiche. Per raggiungere questo obiettivo, definiamo gli aspetti fondamentali dell'attuale sistema economico di crisi e convertiamo a nostro favore i meccanismi che sono alla base del disequilibrio globale.

Il nostro piano è quello di restaurare il maggior livello di giustizia economica possibile, nel seguente modo:
* Utilizzando la nostra moneta digitale (il FairCoin) e sviluppando un sistema economico basato su questa moneta in maniera completamente indipendente dalle banche centrali e dalle istituzioni, restando così fuori dal loro controllo.
* Utilizzando FairCoin in un contesto che ha generalmente giocato contro il sud del mondo, ovvero dentro al sistema di mercato (domanda-offerta), al fine di hackearlo e innestarvi il virus della cooperazione.
* Alimentando la fiducia reciproca attraverso la diffusione della cooperazione ed offrendo incentivi al fine di permettere a tutti e a tutte di investire in un sitema cooperativo, (si rimanda alla strategia della crescita stabile), invece che in un sistema competitivo, instabile e speculativo
* Utilizzando la strategia della crescita stabile per aumentare il valore della moneta, in quanto la disponibilità di unità di valuta è limitata (carattere deflazionistico) e l'aumento della domanda di moneta è in aumento.
* Utilizzando l'aumento di valore della moneta per finanziare progetti e sviluppare strumenti che siano in linea con gli obiettivi di FairCoop, ampliando così in maniera continua l'infrastruttura del sistema.
* Utilizzando tali progetti, strumenti e 'infrastrutture per facilitare l'uso comune di FairCoin al fine di acquistare prodotti e servizi in linea con i principi di FairCoop e quindi aumentare la sua usabilità.
* Utlizzando questo costante aumento di valore della moneta al fine di renderci sempre più indipendenti dalle monete ad emissione governativa (monete FiAt) e ottenere una reale crescita economica, spostandoci sempre più verso un'economia circolare.
* Utilizzando l’economia circolare al fine di rafforzarla tramite l’inclusione di sempre più prodotti e servizi in tutto il mondo, allo scopo di poter soddisfare i nostri bisogni di base e, naturalmente, includere più utenti (individui, gruppi, reti) nel circuito FairCoin.
* Utilizzando il nostro potere collettivo e finanziario derivato dalla costante crescita di utenti e di partecipazione al fine di ridar potere e liberare quante più persone possibile da qualsiasi tipo di oppressione economica o politica
* Utilizzando strumenti molto diversi tra loro e fortemente interconessi, reti umane, servizi, e le caratteristiche di questo sistema decentralizzato ed indipendente come base per un cambiamento sociale diffuso che raggiunga l'intera società globale, noto come: Rivoluzione Integrale. Per questo motivo FairCoop vede nella rivoluzione del sistema economico mondiale il punto di partenza più promettente da cui generare ed iniziare un cambiamento collettivo.