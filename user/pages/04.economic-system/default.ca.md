---
title: 'Sistema Econòmic'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop veu la revolució del sistema econòmic mundialcom el punt de partida més prometedor per a provocar i iniciar un canvi col·lectiu en la societat.

Un dels objectius prioritaris de FairCoop és construir un nou sistema econòmic global basat en la cooperació, l'ètica, la solidaritat i la justícia en les nostres relacions econòmiques. Per aconseguir-ho, hem definit les principals causes del mal funcionament de l'actual sistema econòmic i hem transformat aquests mecanismes que estan provocant el desequilibri global del poder, per a posar-los a favor nostre.

El nostre pla és restaurar el màxim nivell de justícia econòmica mundial que puguem, mitjançant:
* L’ús de la nostra pròpia moneda digital FairCoin i el desenvolupament del seu sistema econòmic propi completament independent dels bancs centrals i les institucions.
* L’ús de FairCoin per a aconseguir una cosa que normalment solia jugar en contra del Sud Global: hackejar les forces del mercat (oferta-demanda) i introduir en elles el virus de la cooperació.
* L’ús del virus de la cooperació per a crear confiança i incentius perquè tothom invertisca en un sistema cooperatiu en lloc de fer-ho enun sistema competitiu volàtil i especulatiu.
* L’ús de l'estratègia del creixement estable per augmentar el valor de la moneda atès que el nombre d'unitats monetàries en circulació és limitat (caràcter deflacionari) i quela seua demandaés creixent.
* L’ús de l'augment del valor de la moneda per finançar el desenvolupament de projectes i eines que s'ajusten als objectius de FairCoop i amplien constantment la infraestructura del sistema.
* L’ús d’aquests projectes, eines i infraestructures per a facilitar l'ús comú de FairCoin per a nous productes i serveis afins amb els Principis de FairCoop, augmentant aixíla seua usabilitat.
* L’ús d’aquest increment constant de la utilitat de FairCoin per a ser cada vegada més global i independent de les monedes fiat i així avançar cap a una economia circular.
* L’ús de l'economia circular, reforçada per incloure cada vegada més productes i serveis a tot el món, per cobrir les nostres necessitats bàsiques i, naturalment, incorporar més usuàries (persones, grups, xarxes) al FairCoin.
* L’ús del nostre poder financer col·lectiu - derivat d'una constant i creixent participació - per a potenciar i alliberar cada vegada a més persones de qualsevol tipus d’exclusió econòmica o política.
* L’ús de les eines, xarxes, serveis i característiques d'aquest sistema descentralitzat independent, molt dispersos però interconnectats, com a base per al canvi global de tota la societat, conegut com a Revolució Integral.