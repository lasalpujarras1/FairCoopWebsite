---
title: 'Ekonomski sistem'
media_order: economicSystem.e9a4e2d2.jpg
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop vidi revoluciju globalnog ekonomskog sistema kao najobećavajuću polaznu tačku za provociranje i iniciranje kolektivne promjene u društvu.

Jedan od prioritetnih ciljeva FairCoop-a je izgraditi novi globalni ekonomski sistem utemeljen na kooperaciji, etici, solidarnosti i pravdi u našim ekonomskim odnosima. Da bi se ovo postiglo, definisali smo glavne uzroke savremene boljke ekonomskog sistema i preokret ovakvih mehanizama, koji uzrokoju globalnu neravnotežu, u našu korist.

Naš plan je da uspostavimo najveći mogući nivo globalne ekonomske pravednosti i to:
* korištenjem naše vlastite digitalne valute FairCoin-a i razvojem vlastitog ekonomskog sistema koji je sasvim nezavisan o centralnim bankama i institucijama.
* korištenje FairCoin-a za nešto što je obično korišteno protiv Globalnog Juga: snage tržišta (ponuda-potražnja), hakirati ih i ubaciti kooperativni virus.
* koristeći kooperativni virus da bi se stvorilo povjerenje i podsticaj za sve da ulažu u kooperativni sistem radije nego u promjenljivi i spekulativni konkurentski sistem.
* koristeći strategiju stabilnog rasta da se poveća vrijednost valute kroz ograničenu ponudu valutnih jedinica (deflaciona osobina) i povećana potražnja za valutom.
* koristeći uvećanu vrijednost valute da se financiraju projekti i razvojni alati koji su povezani sa ciljevima FairCoop-a, te za konstantno proširenje sistemske infrastrukture.
* koristeći takve projekte, alate i infrastrukturu da se olakša uobičajena upotreba FairCoin-a za buduće proizvode i usluge koji su u skladu sa FairCoop Principima i time povećanjem njegove upotrebljivosti.
* koristeći ovu stalno rastuću upotrebljivost FairCoin-a da bi bili sve više nezavisniji od drugih dekretiranih valuta i bliži cirkularnoj ekonomiji.
* koristeći cirkularnu ekonomiju, osnaženu uključivanjem sve više i više proizvoda i usluga, bilo gdje u svijetu, da bi se pokrile naše osnovne potrebe i, naravno, uključivanjem sve više korisnika (pojedinaca, grupa, mreža) na FairCoin.
* koristeći našu zajedničku i financijsku moć izvedenu iz stalno rastuće participacije i broja korisnika, da se osnaži i oslobodi sve više i više ljudi od bilo kakve vrste ekonomskog ili političkog tlačenja.
* koristeći široko rasprostranjene ali međusobno povezane alate, mreže ljudi, usluge i karakteristike ovog nezavisnog decentralizovanog sistema kao bazu društvene promjene cjelokupnog globalnog društva, znano kao integralna revolucija.