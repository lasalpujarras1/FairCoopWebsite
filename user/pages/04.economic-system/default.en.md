---
title: 'Economic System'
taxonomy:
    category:
        - economy
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

> FairCoop sees the revolution of the global economic system as the most promising entry point to provoke and initiate a collective change in society.

One of the priority objectives of FairCoop is to build a new global economic system based on cooperation, solidarity, and justice in our economic relations. To achieve this we defined the major causes of today's ailing economic system and its mechanisms, which are causing the global imbalance of power and how to turn it to our common and global favor.

Our plan is to restore the greatest level of global economic justice that we can, by:

* using our own digital currency FairCoin and developing its own economic system which is completely independent of the central banks and institutions.
* using FairCoin for something that has usually played against the Global South: market forces (supply–demand), hack them and insert the cooperative virus.
* using the cooperative virus to create trust and incentives for everybody to invest in a cooperative system rather than a volatile and speculative competitive system.
* using the stable growth strategy to increase the value of the currency due to the limited supply of currency units (deflationary character) and the increased demand of the currency.
* using the increased value of the currency to finance projects and tools development which are aligned with the goals of FairCoop and constantly extend the infrastructure of the system.
* using those projects, tools and infrastructure to facilitate the common use of FairCoin for further products and services which are in line with the FairCoop Principles, and thus increase its usability.
* using this constantly increasing utility of FairCoin to be globally more and more independent from fiat currencies and move towards a circular economy.
* using the circular economy, strengthened by including more and more products and services worldwide, to cover our basic needs and naturally incorporate more users (individuals, groups, networks) to FairCoin.
* using our collective and financial power derived from constantly growing participation and users to empower and set free more and more people from any kind of economic or political suppression.
* using the widely dispersed but interconnected tools, human networks, services and features of this independent decentralized system as a base for the social change of the whole global society, known as the integral revolution.