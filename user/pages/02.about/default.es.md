---
title: 'Sobre nosotros'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Misión
Nuestra misión es crear un innovador sistema económico glocal desde las bases, en favor de un modelo alternativo y postcapitalista, y allanar así el camino para el cambio colectivo hacia una vida en común. La cooperación, la ética, la solidaridad y la transparencia son factores claves para nosotras a la hora de crear un verdadero sistema de justicia para todas. El desarrollo y el uso de potentes herramientas digitales interconectadas (global) y de redes regionales (local) es crucial para nuestro éxito. Estamos empleando nuestro tiempo y nuestra energía de manera constructiva y colaborativa para crear alternativas reales; estamos llevando las teorías a la práctica.

## Visión
Visualizamos una red global de comunidades y personas autoorganizadas y autoempoderadas, que sea independiente de los abusos de fuerzas y restricciones centralizadas. Nuestro marco y nuestros principios generales se deciden de forma colectiva en nombre del 99%; y al mismo tiempo, se adoptan decisiones a nivel local de acuerdo con las necesidades y circunstancias específicas de cada región. Nuestro trabajo cooperativo está creando un ecosistema independiente cuyo objeto es reducir la desigualdad económica y social a escala global, conduciéndonos hacia una nueva riqueza global, accesible para todas, en forma de commons. Como un efecto dominó, este marco que cambia las reglas del juego del sistema económico abrirá la puerta para una transformación global que tendrá impacto en todos los aspectos de la vida, como la educación, el cuidado de la salud, la energía, la vivienda, el transporte... y que creará oportunidades para redescubrir formas saludables y no explotadoras de relacionarnos entre nosotras y con la Naturaleza.

## Principios
Las acciones de la comunidad FairCoop se basan en tres ramas principales de principios ya existentes: **la Revolución Integral, el modelo colaborativo P2P y la Ética Hacker.**

### 1) Revolución Integral
Es un proceso de relevancia histórica para construir una nueva sociedad autogestionada, basada en la autonomía y en la abolición de toda forma de dominación: el Estado, el capitalismo y cualquier otra que afecte a las relaciones entre los seres humanos y el medio ambiente. Se compromete con la acción consciente, personal y colectiva, para mejorar y recuperar aquellos valores y cualidades que nos permitan llevar una vida en común. Se basa en la construcción de nuevas estructuras y formas de organización en todos los ámbitos de la vida para garantizar la igualdad en la toma de decisiones y la equidad en la satisfacción de nuestras necesidades básicas y vitales. Éstas incluyen:
* Relaciones humanas equitativas basadas en la libertad y la no-coerción
* Autoorganización y asambleas populares soberanas
* Los commons, lo público
	* Recuperar la propiedad común en beneficio de todas, mediante la posesión y el control popular
	* Construir un sistema cooperativo público y autogestionado, basado en la ayuda mutua
	* Liberar el acceso a la información y al conocimiento
* Una nueva economía, basada en la cooperación y la proximidad
* Cooperación con la vida y con la biosfera

<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">Encuentra Más Información Sobre Los Conceptos De La Revolución Integral</a>


### 2) Colaboración P2P
* La colaboración P2P significa que todas asumen ciertos roles y tareas dentro de la comunidad como un compromiso voluntario para el bien común.
* No debe esperarse una reciprocidad directa a través de los compromisos aceptados. Sin embargo, como cada participante tratará de mantener la mutualidad y la solidaridad dentro de los commons, habrá oportunidades para recibir algo desde alguna parte del conjunto.
* Todas las dinámicas de colaboración son no-jerárquicas, por lo que se considera que todas las personas involucradas tienen una voz igual en el proceso.
* Hay mucho más que aprender sobre los principios P2P y sus diferentes subáreas.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Encontrarás Más Información Interesante Aquí</a>



### 3) Ética Hacker
El hacking es más que la imagen social de ciberdelincuentes con capucha negra. La forma en que nosotras y la mayoría de técnicos libres entendemos la acción del hackeo se refiere a los valores morales, la libertad y el desarrollo empleados para mejorar. Las claves de la ética hacker son:
* Toda la información debería ser gratuita
* El acceso a los ordenadores -y a cualquier cosa que pudiera enseñarte algo sobre cómo funciona el mundo- debería ser ilimitado y total
* Desconfía de la autoridad-promueve la descentralización
* Los hackers deberían ser juzgados por su hacking, no por criterios como sus títulos, edad, raza, sexo o posición
* Puede crearse arte y belleza en un ordenador
* Los ordenadores pueden cambiar tu vida para mejor
* No desperdiciar los datos de otras personas
* Hacer públicos los datos disponibles, proteger los datos privados

<a class="block-link" href="https://ccc.de/en/hackerethik">Más Información Sobre La Ética Hacker Aquí</a>