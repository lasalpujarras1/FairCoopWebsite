---
title: 'О ФаирЦооп-у'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Misija
Naša misija je stvaranje inovativnog 'glokalnog' (globalnog i lokalnog) ekonomskog sistema, u korist alternativnog i postkapitalističkog modela, kako bi se otvorio put za kolektivne razmene, u korist uzajamnog života. Saradnja, etika, solidarnost i transparentnost su za nas ključni faktori za izgradnju istinski pravičnog sistema za sve. Razvoj i upotreba moćnih, međusobno povezanih digitalnih instrumenata (globalno) i regionalnih mreža (lokalno) presudni su za naš uspeh. Kako bi smo stvorili prave alternative, sprovodeći teoriju u delo, koristimo sopstveno vreme i energiju, konstruktivno i kroz saradnju.

## Vizija
Zamišljamo globalnu mrežu, sačinjenu od samoorganizovanih i samozaposlenih pojedinaca i lokalnih zajednica, nezavisnih od centralizovanih centara moći i restrikcija koje nas more. Naši globalni principi i okvir su zajednički definisani, u ime 99%, a istovremeno su prihvaćeni na lokalnom nivou, u skladu sa specifičnim potrebama i okolnostima. Tako bi trebalo da ljudi preuzmu kontrolu nad svojim životima, posedujući slobodu izbora između različitih alternativnih društvenih modela, zasnovanih na decentralizovanoj organizacionoj strukturi. Naš kooperativni rad stvara nezavisni ekosistem, sa ciljem da se smanji globalna ekonomska i društvena nejednakost, što vodi ka novim globalnim vrednostima, pristupačnim svima u formi opštih dobara. Okvir ovog ekonomskog sistema kojim se menja igra, kao domino efekat, otvoriće vrata ka globalnom preokretu, utičući na sve aspekte života - obrazovanje, zdravstvo, energetsku efikasnost, komunalije, transport - stvarajući mogućnosti da se ponovo otkriju zdrave i neeksploatisane veze sa ljudskim bićima i prirodom.

## Principi
Delovanje FairCoop zajednice je zasnovano na tri glavna, već postojeća principa: **Integralna revolucija, P2P saradnja, Hakerska etika**

### 1) Integralna revolucija
Ovo je proces od istorijske važnosti za konstrukciju samoupravnog društva, zasnovanog na autonomiji i odsustvu svih oblika dominacije: države, kapitalista i svih onih koji imaju uticaj na društvene veze i prirodnu sredinu. Posvećen je svesnoj, ličnoj i kolektivnoj akciji, za osnaživanje i vraćanje vrednosti i kvaliteta koji nam omogućavaju da zajedno živimo. Zasnovana je na konstrukciji novih struktura i oblika organizacije u svim oblastima života, kako bi se osigurala jednakost u donošenju odluka i sagledavanju naših osnovnih životnih potreba. To podrazumeva:
* Pravične odnose zasnovane na slobodi i bez prinude
* Samoorganizovanje i održavanje kolektivnih skupova
* Opšta dobra i javnost
    * Funkcionisanje kolektivne svojine kao zajedničkog dobra, posredstvom javnog posedovanja i združene kontrole
    * Izgradnju kooperativnih i samoupravnih javnih sistema za uzajamnu podršku
    * Slobodan i nesmetan pristup informacijama i znanju
* Nova ekonomija zasnovana na saradnji i bliskim međuljudskim odnosima
* Ispunjen život i neraskidivu sinergiju sa prirodom


<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">Ovde Pronađi Više O Konceptu Integralne Revolucijel</a>


### 2) P2P saradnja
* P2P saradnja podrazumeva da svako vodi računa o pojedinačnim ulogama i zadacima u okviru zajednice, zbog dobrovoljne posvećenosti opštim dobrima.
* Za posvećenost preuzetim obavezama, ne treba očekivati direktnu nadoknadu. Međutim, kako svaki učesnik pokušava da održi zajedništvo i solidarnosti u okviru opštih dobara, postoji mogućnost i za ličnu dobrobit.
* Dinamika unutar ove saradnje je bez hijerarhije, te svi uključeni imaju jednaka prava.
* Postoji još načina da se sazna više o principima P2P saradnje i njenim različitim oblastima.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Ovde Možete Pronaći Ostale Vredne Informacije</a>



### 3) Hakerska etika
Hakovanje je više od uobičajene slike sajber kriminalaca sa crnim kapuljačama. Ono kako ga mi razumemo, i većina slobodnoumnih ljudi koji se bave novim tehnologijama, odnosi se na moralne vrednosti, slobodu i poboljšanje u korist opšteg dobra. Ključni pojmovi hakerske etike su:
* Sve informacije moraju biti besplatne
* Pristup računarima - i svemu što može učiti o adekvatnom funkcionisanju sveta - treba biti neograničen, nesmetan i potpun. Budite praktični!
* Nepoverenje u vlast - promocija decentralizacije
* O hakerima treba zaključivati na osnovu dela, a ne po kriterijumima poput njihovih diploma, godina, rase, pola ili društvenog položaja
* Upotrebom računara može se umetnički izražavati, stvarajući vredna dela
* Računarima se život može promeniti na bolje
* Ne petljajte sa računarskim podacima drugih ljudi
* Javne podatke učinite dostupnim, a privatne štitite


<a class="block-link" href="https://ccc.de/en/hackerethik">Više Informacija O Hakerskoj Etici</a>