---
title: 'Über uns'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Mission
Unsere Mission ist es, ein innovatives glokales Wirtschaftssystem von unten nach oben zu errichten, das zugunsten eines alternativen und postkapitalistischen Modells, den Weg für einen kollektiven Wandel hin zu einem gemeinschaftlichen Leben ebnet. Zusammenarbeit, Ethik, Solidarität und Transparenz sind für uns Schlüsselfaktoren, um ein wirklich gerechtes System für alle zu schaffen. Die Entwicklung und Nutzung leistungsfähiger vernetzter digitaler Werkzeuge (global) und die Ausweitung regionaler menschlicher Netzwerke (lokal) sind für unseren Erfolg entscheidend. Wir nutzen unsere Zeit und Energie konstruktiv und kooperativ, um durch die Umsetzung von Theorien in die Praxis echte systemrelevante Wahlmöglichkeiten zu schaffen.

## Vision
Wir stellen uns ein globales Netzwerk von selbstorganisierten und selbstbestimmten lokalen Gemeinschaften und Individuen vor, die unabhängig und resistent gegenüber missbräuchlichen geführten zentralisierten Kräften und Restriktionen sind. Unsere globalen Prinzipien und Rahmenbedingungen werden gemeinsam, im Namen aller definiert und gleichzeitig auf lokaler Ebene, entsprechend den regionalspezifischen Bedürfnissen und Gegebenheiten, umgesetzt. Auf diese Weise erhalten die Menschen die Kontrolle über ihr eigenes Leben zurück, indem sie die Freiheit haben, auf der Grundlage einer dezentralisierten Organisationsstruktur verschiedene alternative Gesellschaftsmodelle zu wählen.

Unsere Zusammenarbeit schafft ein unabhängiges Ökosystem, das darauf abzielt, die globale wirtschaftliche und soziale Ungleichheit zu verringern, was zu einem neuen weltweiten Wohlstand führt, der in Form von Gemeingütern für jedermann zugänglich sein wird. Wie ein Dominoeffekt werden die neuen Rahmenbedingungen dieses Wirtschaftssystems neue Möglichkeit für einen globalen Wandel schaffen, der sich auf alle Lebensbereiche, wie beispielsweise Bildung, Gesundheitsfürsorge, Energie, Wohnen und Transport auswirken wird. Dies wird uns die Gelegenheit geben, gesunde und nicht ausbeuterische Beziehungen zu unseren Mitmenschen und der Natur wiederzuentdecken.

## Prinzipien
Die Handlungen der FairCoop-Community basieren auf drei wesentlichen Zweigen bereits existierender Prinzipien: Integrale Revolution, P2P Kollaboration, Hacker Ethik

### 1) Integrale Revolution

Ist ein Prozess von historischer Bedeutung, der auf dem Aufbau einer neuen selbstverwalteten Gesellschaft und der Überwindung aller Formen der Herrschaft (Staat, Kapitalismus) basiert, und sich auf die Beziehungen zwischen Mensch und Umwelt auswirkt. Die Bewegung verpflichtet sich zu bewusstem, persönlichem und kollektivem Handeln für die Verbesserung und Wiedergewinnung der Werte und Qualitäten, die uns ein gemeinschaftliches Leben ermöglichen. Sie beruht auf dem Aufbau neuer Strukturen und Organisationsformen in allen Lebensbereichen, um die Gleichberechtigung in der Entscheidungsfindung und Gerechtigkeit bei der Befriedigung unserer Grund- und Lebensbedürfnisse sicherzustellen. Dazu gehören außerdem:
* Ausgewogene menschliche Beziehungen auf der Grundlage von Freiheit und Zwanglosigkeit
* Selbstorganisation und souveräne Versammlungen
* Die Allgemeinheit, die Öffentlichkeit
    * Wiedererlangung des Gemeinschaftseigentums zum Wohle aller, unter gemeinsamen Besitz und gemeinschaftlicher Verwaltung
	* Aufbau eines kooperativen Systems, das öffentlich und selbstverwaltet ist und auf gegenseitiger Hilfe beruht
	* Freier Zugang zu Informationen und Wissen
* Eine neue Wirtschaft, die auf Kooperation und Nähe basiert.
* Zusammenarbeit mit dem Leben und der Biosphäre.

<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">Erfahre Mehr Über Das Konzept Der Integralen Revolution Hierl</a>


### 2) P2P Kollaboration
* P2P-Kollaboration bedeutet, dass jeder Einzelne bestimmte Rollen und Aufgaben innerhalb der Gemeinschaft als freiwillige Selbstverpflichtung für das Gemeinwohl übernimmt.
* Ein direkter Ausgleich durch die übernommenen Verpflichtungen soll nicht erwartet werden. Da jedoch jeder der TeilnehmerInnen versuchen wird, die Gegenseitigkeit und Solidarität innerhalb der Commons aufrechtzuerhalten, wird es Möglichkeiten geben, etwas vom Ganzen zurückzubekommen.
* Alle Kollaborationsdynamiken sind nicht-hierarchisch, so dass jeder Beteiligte ein gleichberechtigtes Mitspracherecht im Prozess hat.
* Es gibt noch weitaus mehr über die P2P-Kollaboration, deren Grundsätze und die verschiedenen Teilbereiche zu lernen.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Weitere Wertvolle Informationen Dazu Hier</a>



### 3) Hacker Ethik
Hacking ist mehr als nur das gesellschaftliche Bild von Cyberkriminellen mit schwarzen Kapuzenpullis. Wir, und die meisten freien Tech-Leute, verstehen unter Hacking hauptsächlich Aktion, die sich auf moralische Werte, Freiheit und Verbesserungen für das Gute stützen. Die Kernpunkte der Hacker-Ethik sind:
* Alle Informationen sollten frei sein
* Der Zugang zu Computern - und alles, was dir etwas über die Funktionsweise der Welt beibringen könnte - sollte unbegrenzt und vollständig sein.
* Misstraue Autoritäten – fördere Dezentralisierung.
* Hacker sollten nach ihren Aktionen beurteilt werden, und nicht nach oberflächlichen Kriterien wie Aussehen, Alter, Herkunft, Geschlecht oder gesellschaftliche Stellung
* Man kann mit einem Computer Kunst und Schönheit schaffen.* Computer können dein Leben zum Besseren verändern.
* Verunreinigen nicht die Daten anderer Leute.
* Öffentliche Daten verfügbar machen, private Daten schützen.

<a class="block-link" href="https://ccc.de/en/hackerethik">Weitere Informationen Über Hacker-Ethik Gibt Es Hier</a>