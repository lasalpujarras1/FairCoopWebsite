---
title: Oko
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

## Mission
Our mission is to create an innovative glocal economic system from the bottom up in favor of an alternative and post-capitalist model to pave the way for a collective change towards a life in common. Cooperation, solidarity and transparency are key factors for us to create a truly justice system for everyone. The development and use of powerful interconnected digital tools (global) and regional networks (local) are crucial for our success. We are using our time and energy constructively and in a collaborative way to create real alternatives by putting theories into action.

## Vision
We envision a global network of self-organized and self-empowered local communities and individuals, independent of abusive centralized forces and restrictions. Our global principles and framework are defined collectively, on behalf of the 99% and at the same time, are realized at a local level according to their specific regional needs and circumstances. This way people take back control of their own lives by having the freedom to choose diverse alternative society models based on a decentralized organization structure. Our cooperative work is creating an independent ecosystem, which aims to reduce global economic and social inequality, leading to a new global wealth accessible to everybody in the form of commons. Like a domino effect, the game-changing framework of this economic system will open the door for a global shift, impacting all aspects of life such as education, healthcare, energy, housing, transport, and creating opportunities to rediscover healthy and non-exploitative relationships with our fellow human beeings and nature.

## Principles
The actions of the FairCoop community are based on three major branches of already existing principles: Integral Revolution, P2P Collaboration, Hacker Ethics

### 1) Integral Revolution

Is a process of historic significance, for the construction of a new self-managed society, based on autonomy and the abolition of all forms of domination: the state, capitalism, and all other forms that affect relationships both between humans and with the natural environment. It commits itself to conscious action, personal and collective, for the improvement and recovery of the values and qualities that enable us to live a life in common. It is based on the construction of new structures and forms of organization in all areas of life to ensure equality in decision-making and equity in meeting our basic and vital needs. These include:

* Equitable human relations based on freedom and non-coercion
* Self-organization and sovereign popular assemblies
* The commons, the public
	* Recover common ownership for the benefit of all, with popular possession and control
	* Build a cooperative system that is public and self-managed, based upon mutual aid
	* Liberate access to information and knowledge
* A new economy based on cooperation and proximity.
* Cooperation with life and the biosphere.

[Find Out More About The Concepts Of The Integral Revolution](http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/)


### 2) P2P Collaboration

* P2P collaboration means that everybody takes on certain roles and tasks within the community as a voluntary commitment for the common good.
* A direct reciprocity via the commitments accepted should not be expected. However, as each of the participants will try to maintain mutuality and solidarity within the Commons, there will be opportunities to get something back from the whole.

* All collaboration dynamics are non-hierarchical, thus everybody involved is considered to have an equal voice in the process.
* There is way more to learn about the P2P principles and its various sub-areas.

[Further Valuable Information You Will Find Here](https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles)


### 3) Hacker Ethics

Hacking is more than the social picture of cybercriminals with black hoodies. The way we, and most free tech people understands the action of hacking refers to moral values, freedom and improvement for the good. The keys to hacker ethics are:

* All information should be free
* Access to computers—and anything which might teach you something about the way the world works—should be unlimited and total. Always yield to the Hands-On Imperative!
* Mistrust authority—promote decentralization
* Hackers should be judged by their hacking, not criteria such as degrees, age, race, sex, or position
* You can create art and beauty on a computer
* Computers can change your life for the better
* Don't litter other people's data.
* Make public data available, protect private data.

[More Information About Hacker Ethics Here](https://ccc.de/en/hackerethik)
