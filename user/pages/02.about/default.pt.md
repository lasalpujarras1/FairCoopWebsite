---
title: 'Sobre nós'
media_order: about.5264d15e.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

## Missão
A nossa missão é criar um sistema económico inovador global a partir da base, um modelo alternativo e pós-capitalista, e preparar o caminho para a mudança coletiva rumo a uma vida em comum. A cooperação, a ética, a solidariedade e a transparência são factores chave para nós na hora de criar um verdadeiro sistema justo para todos. O desenvolvimento e o uso de potentes ferramentas digitais interconectadas ( global) e as redes regionais ( local) são factores cruciais para o nosso exito. Estamos empregando o nosso tempo e energia de maneira construtiva e colaborativa para criar alternativas reais; nós estamos a levar as teorias para pratica.

## Visão
Visualizamos uma rede global de comunidades e pessoas auto-organizadas e auto-empoderadas, que sejam independentes dos abusos das forças e restrições centralizadas. O nosso objectivo e os nossos principios gerais são decididos de forma colectiva em nome dos 99%; e ao mesmo tempo adoptar decisões ao nível local de acordo com as necessidades e circunstancias especificas de cada região. O nosso trabalho cooperativo está a criar um ecosistema independente cujo objectivo é reduzir a desigualdade económica e social à escala global, conduzindo-nos a uma nova riqueza global, acessivel a todos, como um comum. Como o efeito de dominó, este marco que muda as regras do jogo do sistema economico abrirá a porta para a transformação globlal que terá impacto em todos os aspectos da nossa vida, como a educação, os cuidados de saúde, a energia, a habitação, o transporte ... e que criará oportunidades para redescubrir formas saudáveis e não exploradoras de nos relacionarmos uns com os outros e com a Natureza.

## Princípios
As acções da comunidade FairCoop baseiam-se em três ramos principais de principios já existentes : **a Revolução Integral, o modelo colaborativo P2P e a Ética Hacker.**

### 1) Revolução Integral
É um processo de relevância histórica para construir uma nova sociedade autogerida, baseada na autonomia e na abolição de todas as forma de dominação: o Estado, o capitalismo e qualquer outra que afecte as relações entre os seres humanos e o meio ambiente. 
Está comprometida com a ação consciente, pessoal e coletiva, para melhorar e recuperar os valores e qualidades que nos permitem levar uma vida em comum. Baseia-se na construção de novas estruturas e formas de organização em todas as áreas da vida para garantir a igualdade na tomada de decisões e a equidade na satisfação das nossas necessidades básicas e vitais. Incluindo:

* Relações humanas  equitativas baseadas na liberdade e na não-coersão
* Autoorganização e assembléias populares soberanas 
* Os comuns, o público
		* Recuperar propriedade comum para o benefício de todos, através da posse e controle popular
		* Construir um sistema cooperativo público e autogerido, baseado na ajuda mútua
		* Acesso livre à informação e conhecimento
* Uma nova economia, baseada na cooperação e proximidade
* Cooperação com a vida e a biosfera

<a class="block-link" href="http://integrarevolucio.net/en/integral-revolution/ideological-bases-of-the-call/">Encontra Mais Informação Sobre Os Conceitos Da Revolução Integral</a>



### 2) Colaboração P2P
* Colaboração P2P significa que todos assumem determinados papéis e tarefas dentro da comunidade como um compromisso voluntário para o bem comum.
* Não deve esperar-se uma reciprocidade direta através dos compromissos aceitos. No entanto, como cada participante tentará manter a mutualidade e a solidariedade dentro dos bens comuns, haverá oportunidades de receber algo de alguma parte do todo.
* Toda a dinâmica colaborativa é não hierárquica, por isso considera-se que todas as pessoas envolvidas têm voz igual no processo.
* Há muito mais a aprender sobre os princípios do P2P e suas diferentes subáreas.

<a class="block-link" href="https://wiki.p2pfoundation.net/Core_Peer-2-Peer_Collaboration_Principles">Encontrarás Mais Informação Interessante Aqui</a>



### 3) Ética Hacker
O Hacking é mais do que uma imagem social de cibercriminosos com capuzes negros. A maneira pela qual nós e a maioria dos técnicos livres entendem a ação do hacking refere-se aos valores morais, liberdade e desenvolvimento usados para melhorar. As chaves para a ética hacker são:
* Toda a informação deve ser livre
* O acesso a computadores - e qualquer coisa que possa ensinar algo sobre como o mundo funciona - deve ser ilimitado e total
* Desconfiança da autoridade - promover a descentralização
* Os hackers devem ser julgados pelo seu hacking, não por critérios como os seus títulos, idade, raça, sexo ou posição
* É possível criar Arte e beleza num computador
* Os computadores podem mudar a sua vida para melhor
* Não desperdice dados de outras pessoas
* Tornar públicos os dados disponíveis, proteger os dados privados

<a class="block-link" href="https://ccc.de/en/hackerethik">Mais Informaçõ Sobre A Ética Hacker Aqui</a>