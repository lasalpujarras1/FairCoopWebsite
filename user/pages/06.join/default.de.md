---
title: 'begleiten Sie uns'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Mach mit, lass uns dich in der [Willkommens-Gruppe](https://t.me/askfaircoop) oder im [FairCoop forum](https://forum.fair.coop) kennenlernen und tritt in direkten Kontakt mit uns. Wir suchen kontinuierlich nach Leuten mit diversen Fähigkeiten und Know How, die uns bei der Weiterentwicklung diese wunderschöne globale Bewegung helfen möchten. Hier findest du eine Liste an Fähigkeiten und Fertigkeiten die wir momentan am dringendsten benötigen. Jeder mit eine proaktive und positive Einstellung ist bei uns herzlich willkommen.