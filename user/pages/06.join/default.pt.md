---
title: Coopera
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<p>JUNTA-TE  À FAIRCOOP <a href="https://t.me/askfaircoop">Bem vindo ao nosso grupo no Telegram </a> ou ao <a href="http://forum.fair.coop">FairCoop Forum</a> e entra em contacto directo com um dos membros.
Estamos continuamente à procura de pessoas interessadas que queiram contribuir para o desenvolvimento deste bonito movimento. De qualquer forma, todas aquelas pessoas com uma mentalidade pró-activa e bom espírito são mais que bem vindas ;) </p>

