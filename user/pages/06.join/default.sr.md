---
title: 'Придружи нам се'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Pridružite nam se putem [grupe za dobrodošlicu](https://t.me/askfaircoop) ili na [Forum](https://forum.fair.coop) -u i kontaktirajte nas direktno. Neprestano smo u potrazi za veštim ljudima koji žele da pomognu u razvoju ovog lepo zamišljenog i optimističnog pokreta. Dobrodošao je svako sa pozitivnom energijom i aktivnim stanjem duha :)