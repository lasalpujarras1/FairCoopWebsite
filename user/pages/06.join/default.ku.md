---
title: 'Bi me re bibin'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

[Grûpê Welcome](https://t.me/askfaircoop) an [FairCoop Forum](https://forum.fair.coop) tevlî bibe ji bo me bi raste rast têkilî bikin. Em hertim li pisporan digerin ku dixwazin alîkariya pêşketina vê tevgera xweş bikin. Lîteyek kêrhatî ku em herî lêzgin dixwazin e. Lê belê herkes bi ramanek hestyar û ruhek baş be, bi xêr bê :)