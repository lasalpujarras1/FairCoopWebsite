---
title: Rejoignez-nous
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

Rejoignez-nous dans [notre groupe d'accueil](https://t.me/askfaircoop) ou dans le [Forum](https://forum.fair.coop/) et entrez directement en contact avec nous. Vous voyez ici une liste des compétences dont nous avons le plus besoin de toute urgence. Quoi qu'il en soit, tout le monde avec un état d'esprit proactif et étant de bonne humeur est le bienvenu :)