---
title: 'Unisciti a noi'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Unisciti a noi nel nostro [gruppo di accoglienza telegram](https://t.me/askfaircoop) o sul [forum ufficiale di FairCoop](https://forum.fair.coop/) e entra in contatto diretto con noi. Siamo costantemente alla ricerca di persone qualificate che vogliono aiutare lo sviluppo di questo bellissimo movimento. Comunque, chiunque abbia un atteggiamento propositivo e buon spirito è davvero benvenuto :)