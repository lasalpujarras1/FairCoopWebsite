---
title: Join
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

<p>join us in our <a href="https://t.me/askfaircoop">welcome telegram group</a> or in the <a href="http://forum.fair.coop">FairCoop Forum</a> and get directly in contact with us.
We are constantly looking for skilled people who want to help in developing this beautiful movement.
Anyway, everybody with a proactive mindset and a good spirit is very welcome :)</p>