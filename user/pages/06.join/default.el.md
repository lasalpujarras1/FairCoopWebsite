---
title: 'Ελάτε μαζί μας'
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Συναντήστε μας [στην ομάδα υποδοχής του telegram](https://t.me/askfaircoop) ή στο [FairCoop Forum](http://discourse.fair.coop/) και επικοινωνήστε απ' ευθείας μαζί μας. Αναζητούμε διαρκώς ειδικευμένα άτομα που μπορούν να βοηθήσουν στην ανάπτυξη αυτού του μοναδικού κινήματος. Σε κάθε περίπτωση, κάθε ενεργό και θετικό πνεύμα είναι καλοδεχούμενο :)