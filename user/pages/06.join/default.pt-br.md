---
title: Coopera
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

<p>Para entrar em contato direto conosco, participe do nosso <a href="https://t.me/askfaircoop">grupo de boas-vindas no Telegram </a> ou do <a href="http://forum.fair.coop">Fórum da FairCoop</a>.
Estamos sempre em busca de gente talentosa que queira ajudar esse belo movimento a crescer. Qualquer pessoa com espírito de boa-vontade e iniciativa é mais que bem-vinda! :)</p>

