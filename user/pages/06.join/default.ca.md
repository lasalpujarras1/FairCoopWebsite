---
title: Uneix-te
media_order: join.93e16b83.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

Uneix-te al [grup de benvinguda](https://t.me/askfaircoop) o al [FairCoop foro](http://forum.fair.coop) i entra en contacte amb nosaltres. Cerquem constantment persones qualificades que vulguen ajudar a desenvolupar aquest bonic moviment.